### Tunnel ssh into dei's network

* **ssh -L 5433:dbstud.dei.unipd.it:5432 -L 8081:dbstud.dei.unipd.it:8080 UTENTEDEI@login.dei.unipd.it**

* **ssh dbstud.dei.unipd.it** --> password: account DEI

### From Browser

* **localhost:8081/manager**
    * **Username:** webapp1718
    * **Password:** 1718webapp

### Generate WAR file
* **mvn clean package**

### Access the database from dei's network
* **psql -U webdb -d webapp1718 -p 5433 -h localhost**

* asks password: **[postgres password]**

* for local command:

    * **psql \i 'path/to/ApolloCreateRole.sql'**

    * *\q*

    * **psql -h localhost -p 5433 -U webdb -d apollo**

    * asks for password: **webdb**

    * **psql \i 'path/to/ApolloCreate.sql'**

    * **psql \i 'path/to/ApolloInsert.sql'**
 

## IMPLEMENTED REST FUNCTIONS

* **GET all users**: http://localhost:8080/w2018_apollo/rest/user

* **GET single user**: http://localhost:8080/w2018_apollo/rest/user/{userMail}

* **POST user**: http://localhost:8080/w2018_apollo/rest/user , nel **body** inserire elemento come nell'esempio

* **DELETE user**: http://localhost:8080/w2018_apollo/rest/user/{userMail}

* **PUT user**: http://localhost:8080/w2018_apollo/rest/user/{userMail} , nel **body** inserire elemento come nell'esempio

Body example for a user rest resource:
```json
{
    "user": {
        "email": "ciao@gmail.com",
        "username": "userName",
        "password": "pippo",
        "age": 18,
        "role": "ADMIN"
    }
}
```


* **GET all questions**: http://localhost:8080/w2018_apollo/rest/question

* **GET single question**: http://localhost:8080/w2018_apollo/rest/question/{id}

* **DELETE question**: http://localhost:8080/w2018_apollo/rest/question/{id}

* **GET all questions belonging to a specific category**: http://localhost:8080/w2018_apollo/rest/question/category/{categoryId}
