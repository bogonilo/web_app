<!--
 Copyright 2018 University of Padua, Italy

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Author: Apollo group
 Version: 1.0
 Since: 1.0
-->
<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="./page_component/head.jsp" />
    <title>Apollo | Login</title>
    <link href="<c:url value="/css/login.css"/>" rel="stylesheet">
</head>

<body>

    <%--<div id="header">--%>
    <jsp:include page="./page_component/header.jsp" />


      <div class="container">
    	<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<c:if test='${not empty message && !message.error}'>
                        <div id="login-msg-success" class="isa_success">
                             <i class="fa fa-check"></i>
                             <c:out value="${message.message}"/>
                        </div>
                </c:if>

                <c:if test='${message.error}'>
                      <div id="login-msg-error" class="isa_error">
                             <i class="fa fa-times-circle"></i>
                             <c:out value="${message.message}"/>
                      </div>
                </c:if>

				<div class="panel panel-login">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-6">
								<a href="#" id="login-form-link">Login</a>
							</div>
							<div class="col-xs-6">
								<a href="#" id="register-form-link">Register</a>
							</div>
						</div>
						<hr>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<form id="login-form" action="<c:url value='/login'/>" method="post" role="form" style="display: block;">
									<div class="form-group">
										<input type="email" title="Enter your email" name="user_email" id="user_emailname" tabindex="1" class="form-control" placeholder="User Email" value="" required>
									</div>
									<div class="form-group">
										<input type="password" title="Enter your password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password" required>
									</div>
									<div class="form-group text-center">
										<input type="checkbox" tabindex="3" class="" name="remember" id="remember">
										<label for="remember"> Remember Me</label>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												<input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In">
											</div>
										</div>
									</div>
								</form>
								<form id="register-form" action="<c:url value='/create-user'/>" onsubmit="return checkForm(this)" method="POST" role="form" style="display: none;">
									<div class="form-group">
										<input type="text"  title="Insert your name" name="username" id="username" tabindex="1" class="form-control" placeholder="Username" value="" required>
									</div>
									<div class="form-group">
										<input type="email" title="Insert your email" name="email" id="email" tabindex="1" class="form-control" placeholder="Email Address" value="" required>
									</div>
									<div class="form-group">
										<input type="password" title="Password must contain at least 6 characters, including UPPER/lowercase and numbers" name="password" id="password" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}"  tabindex="2" class="form-control" placeholder="Password">
									</div>
									<div class="form-group">
										<input type="password" title="Must equals to the first password!" name="password2" id="password2" tabindex="2" class="form-control" placeholder="Confirm Password" required>
									</div>
									<div class="form-group">
										<label for="bday">Enter your birthday: (You should be at least 16!) </label>
										<input type="date" id="bday" name="bday" required min="1900-01-01" max="2002-01-01">
										
										<input type="hidden" name="role" id="role" value="user">
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												<input type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Register Now">
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	  </div>


<script type="text/javascript">


    function checkPassword(str)
    {
      var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$/;
      return re.test(str);
    }

    function checkForm(form)
    {
      if(form.username.value == "") {
        alert("Error: Username cannot be blank!");
        form.username.focus();
        return false;
      }
      re = /^\w+$/;
      if(!re.test(form.username.value)) {
        alert("Error: Username must contain only letters, numbers and underscores!");
        form.username.focus();
        return false;
      }

      if(form.password.value == form.password2.value) {
        if(!checkPassword(form.password.value)) {
          alert("The password you have entered is not valid!" +form.password.value + " " + form.password2.value);
          form.password.focus();
          return false;
        }
      } else {
        alert("Error: Please check that you've entered and confirmed your password!");
        alert(form.password2.value);
        form.password.focus();
        return false;
      }
      return true;
    }


</script>


<!-- Footer -->
<jsp:include page="./page_component/footer.jsp" />

<!-- Script -->
<jsp:include page="./page_component/script.jsp" />

<!-- Login Form script -->
<script src="<c:url value="/js/login.js"/>"></script>

</body>
</html>



