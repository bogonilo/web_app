<!--
 Copyright 2018 University of Padua, Italy

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Author: Apollo group
 Version: 1.0
 Since: 1.0
-->

<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Create Interview</title>
	</head>

	<body>
		<div>
        	<div>
        	<div style="display: inline-block; margin-right:3%; vertical-align: middle; line-height: 1;">
           		<a href="/w2018_apollo/jsp/create-user-form.jsp"> Create user</a>
	  		</div>

        	<div style="display: inline-block; margin-right:3%; vertical-align: middle; line-height: 1;">
            	<a href="/w2018_apollo/jsp/create-question-form.jsp"> Create question </a>
        	</div>

        	<div style="display: inline-block; margin-right:3%; vertical-align: middle; line-height: 1;">
                <a href="/w2018_apollo/jsp/create-interview-form.jsp"> Create Interview </a>
            </div>

        	<div style="display: inline-block; margin-right:3%; vertical-align: middle; line-height: 1;">
            	<a href="http://localhost:8081/w2018_apollo/rest/user"> List All User (work with localhost:8081) </a>
        	</div>

        	<div style="display: inline-block; margin-right:3%; vertical-align: middle; line-height: 1;">
                <a href="http://localhost:8081/w2018_apollo/rest/question"> List All Question (work with localhost:8081) </a>
            </div>
		</div>

		<hr>

		<h1>Create Interview</h1>
		<hr/>

		<!-- display the message -->
		<c:import url="/jsp/include/show-message.jsp"/>

		<!-- display the just created interview, if any and no errors -->
		<c:if test='${not empty interview && !message.error}'>
			<ul>
				<li>Title: <c:out value="${interview.title}"/></li>
				<li>Company: <c:out value="${interview.company}"/></li>
				<li>Date: <c:out value="${interview.date}"/></li>
			</ul>
		</c:if>
	</body>
</html>
