<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>

    <c:if test='${not empty question && !message.error}'>
        <title><c:out value="${question.title}"/></title>
    </c:if>
    <jsp:include page="./page_component/head.jsp" />

</head>
<body>

<%--<div id="header">--%>
<jsp:include page="./page_component/header.jsp" />

    <input type="hidden" id="valueH" value="${question.id}">

<div class="container">
  <div class="row content">
    <div class="col-md-12">
        <div class="row">
			<div class="col-md-2 text-center">
			  <img src="<c:url value="/img/user.jpg"/>" class="img-circle" height="65" width="65" alt="Avatar">
			  <h6> <c:out value="${question.userEmail}"/></h6>
			</div>
			<div class="col-md-10">
			  <h3><c:out value="${question.title}"/></h3>
			  <p><c:out value="${question.text}"/></p>
			  <br>
			</div>
      </div>
    </div>
  </div>

	<br/>
	<br/>
	<div id="new_answer"></div>



<div class="row addMargin">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <hr/>
    </div>
    <div class="col-md-2"></div>
</div>

        <div class="row">
			<div id="resultsQuestion"></div>
      </div>
</div>


<!-- Footer -->
<jsp:include page="./page_component/footer.jsp" />
<!-- Answer Script -->
<script src="<c:url value="/js/answers.js"/>"></script>
<script src="<c:url value="/js/login-button.js"/>"></script>

</body>
</html>
