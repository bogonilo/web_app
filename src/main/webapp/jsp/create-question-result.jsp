<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="./page_component/head.jsp" />
    <title>Apollo | Confirm creation</title>
    <meta http-equiv="refresh" content="5; URL=http://localhost:8081/w2018_apollo/jsp/index.jsp" />
</head>

<body>

    <%--<div id="header">--%>
    <jsp:include page="./page_component/header.jsp" />

    <c:if test='${not empty message}'>
    <p><c:out value="${message.error-code}"></c:out></p>
    <p><c:out value="${message.error-detail}"></c:out></p>
</c:if>

	Question not created! </br>
	You will be redirected to the homepage in 5...</br>
	or</br>
	<a href="http://localhost:8081/w2018_apollo/jsp/index.jsp">click here</a>
	 
<!-- Footer -->
<jsp:include page="./page_component/footer.jsp" />

<!-- Script -->
<jsp:include page="./page_component/script.jsp" />

</body>
</html>
