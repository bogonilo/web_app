<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<header>

    <nav class="navbar navbar-expand-md navbar-light bg-light">

        <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
            <a class="navbar-brand btn-light" href="/w2018_apollo/">APOLLO</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="<c:url value="/jsp/index.jsp"/>">Question</a>
                </li>
                <li class="nav-item">
					<a class="nav-link" href="<c:url value="/jsp/create-question-form.jsp"/>">Create Question</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<c:url value="/jsp/interview.jsp"/>">Interview</a>
                </li>
            </ul>
        </div>

        <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
            <ul class="navbar-nav navbar-right">
                <li class="nav-item">
                    <a id="user-hello" class="nav-link" href="#"> </a>
                </li>
                <li class="nav-item">
                    <a id="log-button" class="nav-link" href="/w2018_apollo/login"> Login or Signup </a>
                </li>
            </ul>
        </div>
    </nav>
</header>