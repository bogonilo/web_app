<%--
  Created by IntelliJ IDEA.
  User: lorenzo
  Date: 17/06/18
  Time: 20.35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>

    <c:if test='${not empty interview && !message.error}'>
        <title><c:out value="${interview.title}"/></title>
    </c:if>
    <jsp:include page="./page_component/head.jsp" />

</head>
<body>

<%--<div id="header">--%>
<jsp:include page="./page_component/header.jsp" />

<input type="hidden" id="valueH" value="${interview.id}">

<div class="container">
<div class="row">
    <div class="col-sm">
        <h2><c:out value="${interview.title}"/></h2>
    </div>
</div>
</div>

<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div id="question_list">

        </div>
    </div>
    <div class="col-md-1"></div>
</div>

<!-- Footer -->
<jsp:include page="./page_component/footer.jsp" />

<!-- General Script -->
<jsp:include page="./page_component/script.jsp" />

<script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
<!-- InterviewIntCompany Script -->
<script src="<c:url value="/js/interview.js"/>"></script>


</body>
</html>
