<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="./page_component/head.jsp" />
    <title>Apollo | Home</title>
</head>

<body>

    <%--<div id="header">--%>
    <jsp:include page="./page_component/header.jsp" />



<%--<div id="results">--%>

<%--</div>--%>

<div class="container">

    <form id="searchByCategory">
        <div class="row">
			<div class="col-md-2">
		   		<h5>Search by Category</h5>
			</div>
			<div class="col-md-4">
					<select class="form-control form-control-lg" name="category" id="categoryValue">
						<option value="0">[All]</option>
						<option value="1">Programming</option>
						<option value="2">Data Analisys</option>
						<option value="3">Hacking</option>
						<option value="4">Android development</option>
						<option value="5">OO programming</option>
						<option value="6">Wireless security</option>
					</select>
			</div>
			<div class="col-md-4">
				<button type="submit" id="button_search" class="btn main-btn">
				Search
				</button>
			</div>
		</div>
    </form>

	<br />

	<div class="container containerOver">

		<div class="row center-block">
			<div class="col-md-1"></div>
			<div class="col-md-12">
				<div id="results"></div>
			</div>
			<div class="col-md-1"></div>
		</div>

	</div>
</div>

<!-- Footer -->
<jsp:include page="./page_component/footer.jsp" />

<!-- Script -->
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <jsp:include page="./page_component/script.jsp" />

</body>
</html>
