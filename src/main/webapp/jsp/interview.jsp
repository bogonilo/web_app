<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="./page_component/head.jsp" />
    <title>Apollo | Interview</title>
</head>

<body>

    <%--<div id="header">--%>
    <jsp:include page="./page_component/header.jsp" />


<div class="container containerOver">

   <div class="row text-center">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div id="resultsInter"></div>
            </div>
            <div class="col-md-1"></div>
        </div>

</div>


<!-- Footer -->
<jsp:include page="./page_component/footer.jsp" />

<!-- Script -->
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
<jsp:include page="./page_component/script.jsp" />
<script src="<c:url value="/js/listInterviewIndex.js"/>"></script>

</body>
</html>
