<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="./page_component/head.jsp" />
    <title>Apollo | Create question</title>
	
	<style type="text/css">
		.bs-example{
			margin: 20px;
		}
	</style>
	
</head>

<body>

    <%--<div id="header">--%>
    <jsp:include page="./page_component/header.jsp" />
    
<!--		<div class="col-lg-12">
			<form action="<c:url value='/create-question'/>" method="POST" style="display: block;">
				<div class="form-group">
				<label for="title"> Title: </label>
					<input type="text" name="title" class="form-control" placeholder="Title">
				</div>
				<div class="form-group">
					<textarea type="text" name="text" cols="40" rows="5" class="form-control" placeholder="Write your description."></textarea>
				</div>

				<div id="listCategory">
				
				</div>
				
				<div id="listInterview">
				
				</div>
				
				<input type="hidden" name="userEmail" id="userEmail"/>
				
				

				<button type="submit">Submit</button><br/>


			</form>
			
				
		</div> 
		
<div class="container">
	<div class="row">
		<form role="form" id="contact-form" class="contact-form">
                    <div class="row">
                		<div class="col-md-6">
                  		<div class="form-group">
                            <input type="text" class="form-control" name="Name" autocomplete="off" id="Name" placeholder="Name">
                  		</div>
                  	</div>
                    	<div class="col-md-6">
                  		<div class="form-group">
                            <input type="email" class="form-control" name="email" autocomplete="off" id="email" placeholder="E-mail">
                  		</div>
                  	</div>
                  	</div>
                  	<div class="row">
                  		<div class="col-md-12">
                  		<div class="form-group">
                            <textarea class="form-control textarea" rows="3" name="Message" id="Message" placeholder="Message"></textarea>
                  		</div>
                  	</div>
                    </div>
                    <div class="row">
                    <div class="col-md-12">
                  <button type="submit" class="btn main-btn pull-right">Send a message</button>
                  </div>
                  </div>
                </form>
	</div>
</div>-->
		
<div class="container">
		<form action="<c:url value='/create-question'/>" method="POST" class="bs-example">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<textarea class="form-control textarea" type="text" name="title" rows="2" placeholder="Title"></textarea>
					</div>
				</div>
			</div>
			<br/>
			<div class="row">
				<div class="col-md-6">
						<div id="listInterview">	</div>	
				</div>
			</div>
			<br/>
			<div class="row">
            	<div class="col-md-6">
				  	<div id="listCategory">		</div>
				</div>
			</div>
			<br/>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<textarea class="form-control textarea" type="text" name="text" rows="10" placeholder="Write your description."></textarea>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<input type="hidden" name="userEmail" id="userEmail"/>
				</div>
		  	</div>
                 
			<div class="row">
				<div class="col-md-12">
					<button type="submit" class="btn btn-success pull-right">Create</button>
				</div>
			</div>
                  
    	</form>
</div>
	
<jsp:include page="./page_component/footer.jsp" />

<!-- Bootstrap, Popper, and JQuery JS -->


<script type="text/javascript">
 var elem = document.getElementById("userEmail");
 elem.value = getCookie("useremail");
</script>

<!-- <script src="script.js"></script> -->

<!--<script src="<c:url value="/js/questionList_homepage.js"/>"></script> -->
<script src="<c:url value="/js/createSelectQuestion.js"/>"></script>
<script src="<c:url value="/js/login-button.js"/>"></script>
<script src="<c:url value="/js/utils.js"/>"></script>

</body>
</html>