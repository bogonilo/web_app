<!--
 Copyright 2018 University of Padua, Italy

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Author: Apollo group
 Version: 1.0
 Since: 1.0
-->

<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Create Interview Form</title>
	</head>

  <body>
  		<div>
        	<div>
        	<div style="display: inline-block; margin-right:3%; vertical-align: middle; line-height: 1;">
           		<a href="/w2018_apollo/jsp/create-user-form.jsp"> Create user</a>
	  		</div>

        	<div style="display: inline-block; margin-right:3%; vertical-align: middle; line-height: 1;">
            	<a href="/w2018_apollo/jsp/create-question-form.jsp"> Create question </a>
        	</div>

        	<div style="display: inline-block; margin-right:3%; vertical-align: middle; line-height: 1;">
                 <a href="/w2018_apollo/jsp/create-interview-form.jsp"> Create Interview </a>
            </div>

        	<div style="display: inline-block; margin-right:3%; vertical-align: middle; line-height: 1;">
            	<a href="http://localhost:8081/w2018_apollo/rest/user"> List All User (work with localhost:8081) </a>
        	</div>

        	<div style="display: inline-block; margin-right:3%; vertical-align: middle; line-height: 1;">
                <a href="http://localhost:8081/w2018_apollo/rest/question"> List All Question (work with localhost:8081) </a>
            </div>
		</div>

	<hr>


	<br/><br/>
	<h1>Create Question Form</h1>

	<form method="POST" action="<c:url value='/create-interview'/>">

   		<label for="title"> Title: </label>
   		<input name="title" type="text" /><br><br>

    	<label for="company"> Company: </label>
        <input name="company" type="text" value="1" /><br><br>

        <label for="data"> Data: </label>
        <input name="data" type="date" min="2000-01-02"><br><br>

        <button type="submit">Submit</button><br/>
        <button type="reset">Reset the form</button>

    	<p><a href="/w2018_apollo/jsp/index.jsp"> Return </a></p>

	</form>
</body>
</html>
