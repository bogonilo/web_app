var httpRequest;

var interviewId = document.getElementById('valueH').value;

var url = 'http://localhost:8081/w2018_apollo/rest/interview/'+interviewId;

httpRequest = new XMLHttpRequest();

if (!httpRequest) {
    alert('Giving up :( Cannot create an XMLHTTP instance');
}
httpRequest.onreadystatechange = alertContents;
httpRequest.open('GET', url);
httpRequest.send();

function alertContents() {
    if (httpRequest.readyState === XMLHttpRequest.DONE) {

        if (httpRequest.status == 200) {


            var div = document.getElementById('question_list');

            div.className = '';
            while (div.firstChild) {
                div.removeChild(div.firstChild);
            }

            var table = document.createElement('table');
            table.className = 'table table-striped table-bordered';
            table.id = 'tab_i';
            div.appendChild(table);

            var thead = document.createElement('thead');
            thead.className = 'thead-light';
            table.appendChild(thead);

            var tr = document.createElement('tr');

            var th = document.createElement('th');
            th.appendChild(document.createTextNode('Category'));
            tr.appendChild(th);

            var th = document.createElement('th');
            th.appendChild(document.createTextNode('Title'));
            tr.appendChild(th);

            var th = document.createElement('th');
            th.appendChild(document.createTextNode('Text'));
            tr.appendChild(th);


            thead.appendChild(tr);
            table.appendChild(thead);

            var tbody = document.createElement('tbody');


            var jsonData = JSON.parse(httpRequest.responseText);
            var resource = jsonData['resource-list'];

            if (resource.length == 0) {
                var no_ans = document.createElement('h4');
                no_ans.appendChild(document.createTextNode('No questions yet.'));
                div.appendChild(no_ans);
            }
            else {
                for (var i = 0; i < resource.length; i++) {
                    var question = resource[i].question;

                    var tr = document.createElement('tr');

                    var td_category = document.createElement('td');
                    td_category.appendChild(document.createTextNode(getCategoryName(question['category'])));
                    tr.appendChild(td_category);

                    var td_title = document.createElement('td');
                    var a_question = document.createElement('a');
                    a_question.setAttribute('href', 'http://localhost:8081/w2018_apollo/question/' + question['id']);
                    td_title.appendChild(a_question);
                    a_question.appendChild(document.createTextNode(question['title']));
                    tr.appendChild(td_title);

                    var td_text = document.createElement('td');
                    td_text.appendChild(document.createTextNode(question['text']));
                    tr.appendChild(td_text);


                    tbody.appendChild(tr);
                }

                table.appendChild(tbody);

                $('#tab_i').DataTable();

                div.appendChild(table);
            }

        } else {
            alert('There was a problem with the request. ' + httpRequest.status);
        }
    }
}