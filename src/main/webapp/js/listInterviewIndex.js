var httpRequest;

var url = 'http://localhost:8081/w2018_apollo/rest/interview/';

httpRequest = new XMLHttpRequest();

if (!httpRequest) {
    alert('Giving up :( Cannot create an XMLHTTP instance');
}
httpRequest.onreadystatechange = alertContents;
httpRequest.open('GET', url);
httpRequest.send();

function alertContents() {
    if (httpRequest.readyState === XMLHttpRequest.DONE) {

        if (httpRequest.status == 200) {


            var div = document.getElementById('resultsInter');

            div.className = '';
            while (div.firstChild) {
                div.removeChild(div.firstChild);
            }

            var table = document.createElement('table');
            table.className = 'table table-striped table-bordered';
            table.id = 'tab_';
            div.appendChild(table);

            var thead = document.createElement('thead');
            thead.className = 'thead-light';
            table.appendChild(thead);

            var tr = document.createElement('tr');

            var th = document.createElement('th');
            th.appendChild(document.createTextNode('Title'));
            tr.appendChild(th);

            var th = document.createElement('th');
            th.appendChild(document.createTextNode('Date'));
            tr.appendChild(th);

            var th = document.createElement('th');
            th.appendChild(document.createTextNode('Company'));
            tr.appendChild(th);

            thead.appendChild(tr);
            table.appendChild(thead);

            var tbody = document.createElement('tbody');


            var jsonData = JSON.parse(httpRequest.responseText);
            var resource = jsonData['resource-list'];

            for (var i = 0; i < resource.length; i++) {
                var interview = resource[i].interview;

                var tr = document.createElement('tr');

                var td_title = document.createElement('td');
                var a_question = document.createElement('a');
                a_question.setAttribute("categoryValue",interview['id']);
                a_question.setAttribute('href', 'http://localhost:8081/w2018_apollo/interview/'+interview['id']);
                td_title.appendChild(a_question);
                a_question.appendChild(document.createTextNode(interview['title']));
                tr.appendChild(td_title);

                var td_date = document.createElement('td');
                td_date.appendChild(document.createTextNode(interview['date']));
                tr.appendChild(td_date);

                var td_company = document.createElement('td');
                td_company.appendChild(document.createTextNode(interview['company']));
                tr.appendChild(td_company);

                tbody.appendChild(tr);
            }

            table.appendChild(tbody);

            $('#tab_').DataTable();

            div.appendChild(table);

        } else {
            alert('There was a problem with the request. ' + httpRequest.status);
        }
    }
}