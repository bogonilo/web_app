var httpRequest;
var questionId = document.getElementById("valueH").value;

loadAnswers();

function loadAnswers() {

    var url1 = 'http://localhost:8081/w2018_apollo/rest/answer/question/' + questionId;

    httpRequest = new XMLHttpRequest();

    if (!httpRequest) {
        alert('Giving up :( Cannot create an XMLHTTP instance');

    }

    httpRequest.onreadystatechange = alertContents;
    httpRequest.open('GET', url1);
    httpRequest.send();
}


function alertContents() {
    if (httpRequest.readyState === XMLHttpRequest.DONE) {

        if (httpRequest.status == 200) {

            $('#resultsQuestion').empty(); //clean everything

            var div = document.getElementById('resultsQuestion');

            var jsonData = JSON.parse(httpRequest.responseText);
            var resource = jsonData['resource-list'];

            if (resource.length == 0) {
                var no_ans = document.createElement('h4');
                no_ans.appendChild(document.createTextNode('No answers yet.'));
                div.appendChild(no_ans);
            }

            else {
                for (var i = 0; i < resource.length; i++) {
                    var answer = resource[i].answer;

                    var element ='<div class="row"><div class="col-md-auto"> \n';

                    var element2 = '<img src="http://localhost:8081/w2018_apollo/img/user.jpg" class="img-circle" height="65" width="65" alt="Avatar">\n';
                    var element3 = '</div>\n <div class="col-md-10">\n';

                    var elementHead = '<h5><b>' + answer['userEmail'] + '</b> <small> ' + answer['date'] + '</small></h5>\n';

                    var elementBody = '<p>' + answer['text'] + '</p><br/>\n';

                    var elementEnd = '</div></div> \n';

                    $('#resultsQuestion').append(element + element2 + element3 + elementHead + elementBody + elementEnd);
                }
            }
        }
        else {
            alert('There was a problem with the request.');
        }
    }
}

if(islogged()) {

    var data = new Date();

    $('#new_answer').append('<form class="form-horizontal" id="newAns" method="POST" action="http://localhost:8081/w2018_apollo/create-answer"> \n' +
        '<div class="form-group">' +
        '\n' +
        '    <input name="question" id="question" type="hidden" value="'+ questionId +'">\n' +
        '\n' +
        '    <input name="userEmail" id="userEmail" type="hidden" value="' + getCookie("useremail")+ '">\n' +
        '\n' +
        '    <div class="form-group">\n' +
        '    <label for="text">Insert your answer here:</label>\n' +
        '    <textarea class="form-control" name="text" id="text" rows="3"></textarea>\n' +
        '  </div>' +
        '\n' +
        '    <input name="date" id="date" type="hidden" value="' + data.getFullYear() +'-' + (data.getUTCMonth()+1) + '-' + data.getUTCDate() + '">\n' +
        '\n' +
        //'    <button type="submit">Submit</button>\n' +
        //'    <button type="reset">Clear</button>\n' +
        '    <button type="submit" class="btn btn-success">Send</button>\n' +
        '    <button type="reset" class="btn btn-danger">Clear</button>\n' +
        '\n' +
        '</div>' +
        '</form>');
}
else {
    $('#new_answer').append('<p>Log in to insert a new answer.</p>');
}


function reload(msg)
{
    alert('reload chiamato' + msg);
}