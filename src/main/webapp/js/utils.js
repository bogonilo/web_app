//window.onload = prova();

//function prova(){
//    alert("utils load");
//}


function islogged(){

        var cookie = getCookie("username");

        if ( cookie != "") {
            return true;
        } else {
            return false;
        }
}

function getCookie(cname) {
	    var name = cname + "=";
	    var ca = document.cookie.split(';');
	    for(var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
		    c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
		    return c.substring(name.length, c.length);
		}
	    }

	    return "";
}

function getCategoryName(cat) {

    switch(cat)
    {
        case 1: return "Programming";
        case 2: return "Data Analysis";
        case 3: return "Hacking";
        case 4: return "Android development";
        case 5: return "OO programming";
        case 6: return "Wireless security";
    }
}