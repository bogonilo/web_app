var httpRequestCat;

var urlCat = 'http://localhost:8081/w2018_apollo/rest/category/';

httpRequestCat = new XMLHttpRequest();

if (!httpRequestCat) {
    alert('Giving up :( Cannot create an XMLHTTP instance');
}
httpRequestCat.onreadystatechange = alertContentsCat;
httpRequestCat.open('GET', url);
httpRequestCat.send();

function alertContentsCat() {
    if (httpRequestCat.readyState === XMLHttpRequest.DONE) {

        if (httpRequestCat.status == 200) {


            var divCat = document.getElementById('createSelectCategory');

            divCat.className = '';
            while (divCat.firstChild) {
                divCat.removeChild(divCat.firstChild);
            }

            var selectCat = document.createElement('select');
            selectCat.className = 'select';
			selectCat.setAttribute("class","form-control form-control-lg");
			selectCat.setAttribute("name","category");
            divCat.appendChild(selectCat);

            var jsonDataCat = JSON.parse(httpRequestCat.responseText);
            var resourceCat = jsonDataCat['resource-list'];
			
			var optionCat = document.createElement('option');
			optionCat.value = 0;
			optionCat.innerHTML = "[All]";
			selectCat.appendChild(optionCat);


            for (var iCat = 0; iCat < resourceCat.length; iCat++) {
                var categoryCat = resourceCat[iCat].category;

                var optionCat = document.createElement('option');
				optionCat.value = categoryCat['id'];
                optionCat.innerHTML = categoryCat['name'];
                selectCat.appendChild(optionCat);

            }

            divCat.appendChild(selectCat);

        } else {
            alert('There was a problem with the request. ' + httpRequest.status);
        }
    }
}