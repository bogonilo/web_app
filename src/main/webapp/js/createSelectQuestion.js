var httpRequest;
var httpRequest2;

var url = 'http://localhost:8081/w2018_apollo/rest/category/';
var url2 = 'http://localhost:8081/w2018_apollo/rest/interview/';

httpRequest = new XMLHttpRequest();
httpRequest2 = new XMLHttpRequest();

if (!httpRequest || !httpRequest2) {
    alert('Giving up :( Cannot create an XMLHTTP instance');
}

httpRequest.onreadystatechange = alertContents;
httpRequest2.onreadystatechange = alertContents2;
httpRequest.open('GET', url);
httpRequest2.open('GET', url2);
httpRequest.send();
httpRequest2.send();

function alertContents() {
    if (httpRequest.readyState === XMLHttpRequest.DONE) {

        if (httpRequest.status == 200) {
            var div = document.getElementById('listCategory');

            div.className = '';
            while (div.firstChild) {
                div.removeChild(div.firstChild);
            }

            var select = document.createElement('select');
            select.className = 'select';
			select.setAttribute("class","form-control form-control-lg");
			select.setAttribute("name","category");
            div.appendChild(select);

            var jsonData = JSON.parse(httpRequest.responseText);
            var resource = jsonData['resource-list'];

            for (var i = 0; i < resource.length; i++) {
                var category = resource[i].category;

                var option = document.createElement('option');
				option.value = category['id'];
                option.innerHTML = category['name'];
                select.appendChild(option);
            }

            div.appendChild(select);

        } else {
            alert('There was a problem with the request. ' + httpRequest.status);
        }
    }
}

function alertContents2() {
    if (httpRequest2.readyState === XMLHttpRequest.DONE) {

        if (httpRequest2.status == 200) {
            var div2 = document.getElementById('listInterview');

            div2.className = '';
            while (div2.firstChild) {
                div2.removeChild(div2.firstChild);
            }

            var select2 = document.createElement('select');
			select2.className = 'select';
			select2.setAttribute("class","form-control form-control-lg");
			select2.setAttribute("name","interview");
            div2.appendChild(select2);

            var jsonData2 = JSON.parse(httpRequest2.responseText);
            var resource2 = jsonData2['resource-list'];

            for (var j = 0; j < resource2.length; j++) {
                var interview = resource2[j].interview;

                var option2 = document.createElement('option');
				option2.value = interview['id'];
                option2.innerHTML = interview['title'];
                select2.appendChild(option2);
            }

            div2.appendChild(select2);

        } else {
            alert('There was a problem with the request. ' + httpRequest.status);
        }
    }
}