(function() {

    var httpRequest;

    document.getElementById('searchByCategory').addEventListener('submit', function(event) {

        var url = "http://localhost:8081/w2018_apollo/rest/question/";

        var category = document.getElementById('categoryValue').value;

        if(category!=="0"){
            url = url+"category/"+category;
        }
        httpRequest = new XMLHttpRequest();

        if (!httpRequest) {
            alert('Giving up :( Cannot create an XMLHTTP instance');
        }
        httpRequest.onreadystatechange = alertContents;
        httpRequest.open('GET', url);
        httpRequest.send();
        event.preventDefault();
    });

    function alertContents() {
        if (httpRequest.readyState === XMLHttpRequest.DONE) {

            if (httpRequest.status == 200) {


                var div = document.getElementById('results');

                div.className = '';
                while (div.firstChild) {
                    div.removeChild(div.firstChild);
                }

                var table = document.createElement('table');
                table.className = 'table table-striped table-bordered';
                table.id = 'tab';
                div.appendChild(table);

                var thead = document.createElement('thead');
                thead.className = 'thead-light';
                table.appendChild(thead);

                var tr = document.createElement('tr');

                var th = document.createElement('th');
                th.appendChild(document.createTextNode('Category'));
                tr.appendChild(th);

                var th = document.createElement('th');
                th.appendChild(document.createTextNode('Title'));
                tr.appendChild(th);

                var th = document.createElement('th');
                th.appendChild(document.createTextNode('Interview'));
                tr.appendChild(th);

                var th = document.createElement('th');
                th.appendChild(document.createTextNode('User'));
                tr.appendChild(th);

                thead.appendChild(tr);
                table.appendChild(thead);

                var tbody = document.createElement('tbody');


                var jsonData = JSON.parse(httpRequest.responseText);
                var resource = jsonData['resource-list'];

                for (var i = 0; i < resource.length; i++) {
                    var question = resource[i].question;

                    var tr = document.createElement('tr');

                    var td_category = document.createElement('td');
                    td_category.appendChild(document.createTextNode(getCategoryName(question['category'])));
                    tr.appendChild(td_category);

                    var td_title = document.createElement('td');
                    var a_question = document.createElement('a');
                    a_question.setAttribute('href', 'http://localhost:8081/w2018_apollo/question/'+question['id'])
                    td_title.appendChild(a_question);
                    a_question.appendChild(document.createTextNode(question['title']));
                    tr.appendChild(td_title);

                    var td_interview = document.createElement('td');
                    var a_interview = document.createElement('a');
                    //a_question.setAttribute("categoryValue",interview['id']);
                    a_interview.setAttribute('href', 'http://localhost:8081/w2018_apollo/interview/'+question['interviewId']);
                    td_interview.appendChild(a_interview);
                    a_interview.appendChild(document.createTextNode(question['interviewTitle']));
                    tr.appendChild(td_interview);

                    var td_user = document.createElement('td');
                    td_user.appendChild(document.createTextNode(question['userEmail']));
                    tr.appendChild(td_user);


                    tbody.appendChild(tr);
                }

                table.appendChild(tbody);

                $('#tab').DataTable();

                div.appendChild(table);

            } else {
                alert('There was a problem with the request. ' + httpRequest.status);
            }
        }
    }
})();