var httpRequest;

var url = 'http://localhost:8081/w2018_apollo/rest/category/';

httpRequest = new XMLHttpRequest();

if (!httpRequest) {
    alert('Giving up :( Cannot create an XMLHTTP instance');
}
httpRequest.onreadystatechange = alertContents;
httpRequest.open('GET', url);
httpRequest.send();

function alertContents() {
    if (httpRequest.readyState === XMLHttpRequest.DONE) {

        if (httpRequest.status == 200) {


            var div = document.getElementById('listCategory');

            div.className = '';
            while (div.firstChild) {
                div.removeChild(div.firstChild);
            }

            var select = document.createElement('select');
            select.className = 'select';
			select.setAttribute("name","category");
            div.appendChild(select);

            var jsonData = JSON.parse(httpRequest.responseText);
            var resource = jsonData['resource-list'];

            for (var i = 0; i < resource.length; i++) {
                var category = resource[i].category;

                var option = document.createElement('option');
				option.value = category['id'];
                option.innerHTML = category['name'];
                select.appendChild(option);

            }

            div.appendChild(select);

        } else {
            alert('There was a problem with the request. ' + httpRequest.status);
        }
    }
}