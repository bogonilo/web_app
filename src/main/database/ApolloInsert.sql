--  Copyright 2018 University of Padua, Italy
--
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--
--      http://www.apache.org/licenses/LICENSE-2.0
--
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--
--  Author: Apollo group
--  Version: 1.0
--  Since: 1.0

-- #################################################################################################
-- ## Database insertions                                                                         ##
-- #################################################################################################

--
-- Insertion into the user_account table
--
INSERT INTO apollo.user_account (email, username, password, birthday, role) VALUES('rossi1@gmail.com', 'rossi1', 'Rossi1_123', '1994-02-02', 'USER');
INSERT INTO apollo.user_account (email, username, password, birthday, role) VALUES('rossi2@gmail.com', 'rossi2', 'Rossi2_123', '1994-12-22', 'USER');
INSERT INTO apollo.user_account (email, username, password, birthday, role) VALUES('rossi3@gmail.com', 'rossi3', 'Rossi3_123', '1993-12-06', 'USER');
INSERT INTO apollo.user_account (email, username, password, birthday, role) VALUES('rossi4@gmail.com', 'rossi4', 'Rossi4_123', '1996-02-02', 'USER');
INSERT INTO apollo.user_account (email, username, password, birthday, role) VALUES('admin@admin.com', 'admin', 'Admin123', '1954-02-02', 'USER');

--
-- Insertion into the company table
--
INSERT INTO apollo.company (id, name, address) VALUES(1, 'Google', 'USA');
INSERT INTO apollo.company (id, name, address) VALUES(2, 'Facebook', 'USA');
INSERT INTO apollo.company (id, name, address) VALUES(3, 'IT Robotics', 'ITA');
INSERT INTO apollo.company (id, name, address) VALUES(4, 'Instagram', 'USA');

--
-- Insertion into the category table
--
INSERT INTO apollo.category (id, name) VALUES(1, 'Programming');
INSERT INTO apollo.category (id, name) VALUES(2, 'Data Analisys');
INSERT INTO apollo.category (id, name) VALUES(3, 'Hacking');
INSERT INTO apollo.category (id, name) VALUES(4, 'Android development');
INSERT INTO apollo.category (id, name) VALUES(5, 'OO programming');
INSERT INTO apollo.category (id, name) VALUES(6, 'Wireless security');

--
-- Insertion into the interview table
--
INSERT INTO apollo.interview (id, company, date, title) VALUES(1, 1, '22/05/2016', 'Google interview');
INSERT INTO apollo.interview (id, company, date, title) VALUES(2, 4, '25/02/2017', 'Instagram interview');
INSERT INTO apollo.interview (id, company, date, title) VALUES(3, 4, '13/04/2017', 'Instagram programming interview');
INSERT INTO apollo.interview (id, company, date, title) VALUES(4, 3, '11/02/2018', 'IT Robotics first interview');
INSERT INTO apollo.interview (id, company, date, title) VALUES(5, 3, '06/03/2018', 'IT Robotics programming interview');

--
-- Insertion into the question table
--
INSERT INTO apollo.question (id, category, title, text, interview, userEmail) VALUES(1, 1, 'Question about programming', 'How can we compile a program?', 1, 'rossi1@gmail.com');

--
-- Insertion into the topic table
--
INSERT INTO apollo.topic (id, category, name) VALUES(1, 1, 'C++');
INSERT INTO apollo.topic (id, category, name) VALUES(2, 1, 'C#');
INSERT INTO apollo.topic (id, category, name) VALUES(3, 1, 'Java');

--
-- Insertion into the refer table
--
INSERT INTO apollo.refer (question, topic) VALUES(1, 1);
INSERT INTO apollo.refer (question, topic) VALUES(1, 2);
INSERT INTO apollo.refer (question, topic) VALUES(1, 3);

--
-- Insertion into the answer table
--
INSERT INTO apollo.answer (id, userEmail, question, text, date) VALUES(1, 'rossi2@gmail.com', 1, 'The answer of everything is 42! -> 1.0', '16/03/2018');
INSERT INTO apollo.answer (id, userEmail, question, text, date) VALUES(2, 'rossi3@gmail.com', 1, 'The answer of everything is 42! -> 2.0', '17/03/2018');

--
-- Insertion into the rate table
--
INSERT INTO apollo.rate (userEmail, answer, value) VALUES('rossi1@gmail.com', 1, 8);
INSERT INTO apollo.rate (userEmail, answer, value) VALUES('rossi1@gmail.com', 2, 9);
INSERT INTO apollo.rate (userEmail, answer, value) VALUES('rossi4@gmail.com', 2, 10);

--
-- Insertion into the comment table
--
INSERT INTO apollo.comment (id, answer, userEmail, text, date) VALUES(1, 1, 'rossi2@gmail.com', 'I think the answer of everything is 42', '13/04/2018');

---------------------------------------------------------------------------------------------------------------------------
