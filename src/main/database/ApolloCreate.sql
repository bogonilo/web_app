--  Copyright 2018 University of Padua, Italy
--
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at
--
--      http://www.apache.org/licenses/LICENSE-2.0
--
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.
--
--  Author: Apollo group
--  Version: 1.0
--  Since: 1.0


-- #################################################################################################
-- ## Creation of a schema to avoid name clashes                                                  ##
-- #################################################################################################

-- Drop the apollo schema, if exists, and any object within it
DROP SCHEMA IF EXISTS apollo CASCADE;

-- Create the apollo schema
CREATE SCHEMA apollo AUTHORIZATION webdb;

-----------------------------------------------------------------------------

DROP TABLE IF EXISTS apollo.user_account CASCADE;
DROP TABLE IF EXISTS apollo.category CASCADE;
DROP TABLE IF EXISTS apollo.company CASCADE;
DROP TABLE IF EXISTS apollo.interview CASCADE;
DROP TABLE IF EXISTS apollo.question CASCADE;
DROP TABLE IF EXISTS apollo.topic CASCADE;
DROP TABLE IF EXISTS apollo.refer CASCADE;
DROP TABLE IF EXISTS apollo.answer CASCADE;
DROP TABLE IF EXISTS apollo.rate CASCADE;
DROP TABLE IF EXISTS apollo.comment CASCADE;

-- #################################################################################################
-- ## Creation of the tables                                                                      ##
-- #################################################################################################

--
-- This table represents the user accounts
--
CREATE TABLE apollo.user_account(
    email VARCHAR(200),
    username VARCHAR(100) NOT NULL UNIQUE,
    password VARCHAR(32) NOT NULL,
    birthday DATE NOT NULL,
    role VARCHAR(100) NOT NULL,
    PRIMARY KEY(email)
);

--
-- This table represents the question categories
--
CREATE TABLE apollo.category(
    id SERIAL,
    name VARCHAR(500) NOT NULL,
    PRIMARY KEY(id)
);

--
-- This table represents the companies
--
CREATE TABLE apollo.company(
    id SERIAL,
    name VARCHAR(200) NOT NULL UNIQUE,
    address VARCHAR(200) NOT NULL,
    PRIMARY KEY(id)
);

--
-- This table represents the interviews during which the questions were asked
--
CREATE TABLE apollo.interview(
    id SERIAL,
    company INTEGER NOT NULL,
    date DATE NOT NULL,
    title VARCHAR(200) NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(company) REFERENCES apollo.company(id)
);

--
-- This table represents the questions
--
CREATE TABLE apollo.question(
    id SERIAL,
    category INTEGER NOT NULL,
    title VARCHAR(400) NOT NULL,
    text VARCHAR(1000) NOT NULL,
    interview INTEGER NOT NULL,
    userEmail VARCHAR(200) NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(category) REFERENCES apollo.category(id),
    FOREIGN KEY(interview) REFERENCES apollo.interview(id),
    FOREIGN KEY(userEmail) REFERENCES apollo.user_account(email)
);

--
-- This table represents the topics
--
CREATE TABLE apollo.topic(
    id SERIAL,
    category INTEGER NOT NULL,
    name VARCHAR(500) NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(category) REFERENCES apollo.category(id)
);

--
-- This table represents the connection between a question and a topic
--
CREATE TABLE apollo.refer(
    question INTEGER NOT NULL,
    topic INTEGER NOT NULL,
    PRIMARY KEY(question, topic),
    FOREIGN KEY(question) REFERENCES apollo.question(id),
    FOREIGN KEY(topic) REFERENCES apollo.topic(id)
);

--
-- This table represents the answers
--
CREATE TABLE apollo.answer(
    id SERIAL,
    userEmail VARCHAR(255) NOT NULL,
    question INTEGER NOT NULL,
    text VARCHAR(500) NOT NULL,
    date DATE NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(userEmail) REFERENCES apollo.user_account(email),
    FOREIGN KEY(question) REFERENCES apollo.question(id)
);

--
-- This table represents the rates of an answer
--
CREATE TABLE apollo.rate(
    userEmail VARCHAR(255) NOT NULL,
    answer INTEGER NOT NULL,
    value INTEGER NOT NULL,
    PRIMARY KEY(userEmail, answer),
    FOREIGN KEY(userEmail) REFERENCES apollo.user_account(email),
    FOREIGN KEY(answer) REFERENCES apollo.answer(id)
);

--
-- This table represents the comments
--
CREATE TABLE apollo.comment(
    id SERIAL,
    answer INTEGER NOT NULL,
    userEmail VARCHAR(255) NOT NULL,
    text VARCHAR(500) NOT NULL,
    date DATE NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(answer) REFERENCES apollo.answer(id),
    FOREIGN KEY(userEmail) REFERENCES apollo.user_account(email)
);
