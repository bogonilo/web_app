package it.unipd.dei.webapp.rest;

import it.unipd.dei.webapp.database.ListCategoryDatabase;
import it.unipd.dei.webapp.resource.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.util.List;

/**
 * Manages the REST API for the {@link Category} resource.
 *
 * @author Apollo group
 * @version 1.00
 * @since 1.00
 */
public class CategoryRestResource extends RestResource{

    /**
     * Creates a new REST resource for managing {@code Category} resources.
     *
     * @param req the HTTP request.
     * @param res the HTTP response.
     * @param con the connection to the database.
     */
    public CategoryRestResource(final HttpServletRequest req, final HttpServletResponse res, Connection con) {
        super(req, res, con);
    }

    /**
     * This method return all the categories saved into the database
     * @throws IOException if any error occurs in the client/server communication.
     */
    public void listCategory() throws IOException {

        List<Category> cl  = null;
        Message m = null;

        try{
            // creates a new object for accessing the database and lists all the categories
            cl = new ListCategoryDatabase(con).listCategory();

            if(cl != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                new ResourceList(cl).toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new Message("Cannot list category: unexpected error.", "E5A1", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t)
        {
            m = new Message("Cannot search list: unexpected error.", "E5A1", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }
}


