/*
 * Copyright 2018 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.rest;

import it.unipd.dei.webapp.database.*;
import it.unipd.dei.webapp.resource.Message;
import it.unipd.dei.webapp.resource.ResourceList;
import it.unipd.dei.webapp.resource.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Manages the REST API for the {@link User} resource.
 *
 * @author Apollo group
 * @version 1.00
 * @since 1.00
 */
public final class UserRestResource extends RestResource {

	/**
	 * Creates a new REST resource for managing {@code User} resources.
	 *
	 * @param req the HTTP request.
	 * @param res the HTTP response.
	 * @param con the connection to the database.
	 */
	public UserRestResource(final HttpServletRequest req, final HttpServletResponse res, Connection con) {
		super(req, res, con);
	}


	/**
	 * Creates a new user into the database.
	 *
	 * @throws IOException
	 *             if any error occurs in the client/server communication.
	 */
	public void createUser() throws IOException {

        User u = null;
        Message m = null;

		try{

			final User user =  User.fromJSON(req.getInputStream());

			// creates a new object for accessing the database and stores the user
			u= new CreateUserDatabase(con, user).createUser();

			if(u!= null) {
				res.setStatus(HttpServletResponse.SC_CREATED);
				u.toJSON(res.getOutputStream());
			} else {
				// it should not happen
				m = new Message("Cannot create the user: unexpected error.", "E5A1", null);
				res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				m.toJSON(res.getOutputStream());
			}
		} catch (Throwable t) {
			if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23505")) {
				m = new Message("Cannot create the user: it already exists.", "E5A2", t.getMessage());
				res.setStatus(HttpServletResponse.SC_CONFLICT);
				m.toJSON(res.getOutputStream());
			} else {
				m = new Message("Cannot create the user: unexpected error.", "E5A1", t.getMessage());
				res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				m.toJSON(res.getOutputStream());
			}
		}
	}

	/**
	 * Reads an user from the database.
	 *
	 * @throws IOException
	 *             if any error occurs in the client/server communication.
	 */
	public void readUser() throws IOException {

		User u = null;
		Message m = null;

		try{
			// parse the URI path to extract the email
			String path = req.getRequestURI();
			path = path.substring(path.lastIndexOf("user") + 4);
			final String email = path.substring(1);


			// creates a new object for accessing the database and reads the user
			u= new ReadUserDatabase(con, email).readUser();

			if(u!= null) {
				res.setStatus(HttpServletResponse.SC_OK);
				u.toJSON(res.getOutputStream());
			} else {
				m = new Message(String.format("User %d not found.", email), "E5A3", null);
				res.setStatus(HttpServletResponse.SC_NOT_FOUND);
				m.toJSON(res.getOutputStream());
			}
		} catch (Throwable t) {
			m = new Message("Cannot read user: unexpected error.", "E5A1", t.getMessage());
			res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			m.toJSON(res.getOutputStream());
		}
	}

	/**
	 * Updates an user in the database.
	 *
	 * @throws IOException
	 *             if any error occurs in the client/server communication.
	 */
	public void updateUser() throws IOException {

		User u = null;
		Message m = null;

		try{
			// parse the URI path to extract the email
			String path = req.getRequestURI();
			path = path.substring(path.lastIndexOf("user/") + 4);
			final String email = path.substring(1);
			final User user =  User.fromJSON(req.getInputStream());
			if (!email.equals(user.getEmail())) {
				m = new Message(
						"Wrong request for URI /user/{email}: URI request and user resource emails differ.",
						"E4A7", String.format("Request URI email %d; user resource email %d.",
											  email, user.getEmail()));
				res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				m.toJSON(res.getOutputStream());
				return;
			}

			// creates a new object for accessing the database and updates the user
			u= new UpdateUserDatabase(con, user).updateUser();

			if(u!= null) {
				res.setStatus(HttpServletResponse.SC_OK);
				u.toJSON(res.getOutputStream());
			} else {
				m = new Message(String.format("User %d not found.", user.getEmail()), "E5A3", null);
				res.setStatus(HttpServletResponse.SC_NOT_FOUND);
				m.toJSON(res.getOutputStream());
			}
		} catch (Throwable t) {
			if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23503")) {
				m = new Message("Cannot update the user: other resources depend on it.", "E5A4", t.getMessage());
				res.setStatus(HttpServletResponse.SC_CONFLICT);
				m.toJSON(res.getOutputStream());
			} else {
				m = new Message("Cannot update the user: unexpected error.", "E5A1", t.getMessage());
				res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				m.toJSON(res.getOutputStream());
			}
		}
	}


	/**
	 * Deletes an user from the database.
	 *
	 * @throws IOException
	 *             if any error occurs in the client/server communication.
	 */
	public void deleteUser() throws IOException {

		User u = null;
		Message m = null;

		try{
			// parse the URI path to extract the email
			String path = req.getRequestURI();
			path = path.substring(path.lastIndexOf("user/") + 4);

			final String email = path.substring(1); //todo : è giusto fare il parse così?

			// creates a new object for accessing the database and deletes the user
			u= new DeleteUserDatabase(con, email).deleteUser();

			if(u!= null) {
				res.setStatus(HttpServletResponse.SC_OK);
				u.toJSON(res.getOutputStream());
			} else {
				m = new Message(String.format("User %d not found.", email), "E5A3", null);
				res.setStatus(HttpServletResponse.SC_NOT_FOUND);
				m.toJSON(res.getOutputStream());
			}
		} catch (Throwable t) {
			if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23503")) {
				m = new Message("Cannot delete the user: other resources depend on it.", "E5A4", t.getMessage());
				res.setStatus(HttpServletResponse.SC_CONFLICT);
				m.toJSON(res.getOutputStream());
			} else {
				m = new Message("Cannot delete the user: unexpected error.", "E5A1", t.getMessage());
				res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				m.toJSON(res.getOutputStream());
			}
		}
	}
}
