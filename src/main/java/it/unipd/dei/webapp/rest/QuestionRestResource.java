package it.unipd.dei.webapp.rest;

import it.unipd.dei.webapp.database.*;
import it.unipd.dei.webapp.resource.Message;
import it.unipd.dei.webapp.resource.AbstractQuestion;
import it.unipd.dei.webapp.resource.QuestionIntInterview;
import it.unipd.dei.webapp.resource.ResourceList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Manages the REST API for the {@link AbstractQuestion} resource.
 *
 * @author Apollo group
 * @version 1.00
 * @since 1.00
 */
public class QuestionRestResource extends RestResource{

    /**
     * Creates a new REST resource for managing {@code AbstractQuestion} resources.
     *
     * @param req the HTTP request.
     * @param res the HTTP response.
     * @param con the connection to the database.
     */
    public QuestionRestResource(final HttpServletRequest req, final HttpServletResponse res, Connection con) {
        super(req, res, con);
    }

    /**
     * Create new question into the database
     * @throws IOException if any error occurs in the client/server communication.
     */
    public void createQuestion() throws IOException {

        //todo vediamo se lasciare con rest la creazione della question o gestirla da jsp
        AbstractQuestion q = null;
        Message m = null;

        try{
            final QuestionIntInterview question  = QuestionIntInterview.fromJSON(req.getInputStream());

            // creates a new object for accessing the database and stores the question
            q = new CreateQuestionDatabase(con, question).createQuestion();

            if(q != null) {
                res.setStatus(HttpServletResponse.SC_CREATED);
                q.toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new Message("Cannot create the question: unexpected error.", "E5A1", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }catch (Throwable t){
            if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23505")) {
                m = new Message("Cannot create the question: it already exists.", "E5A2", t.getMessage());
                res.setStatus(HttpServletResponse.SC_CONFLICT);
            } else {
                m = new Message("Cannot create the question: unexpected error.", "E5A1", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }

    /**
     * This method return all the questions saved into the database
     * @throws IOException if any error occurs in the client/server communication.
     */
    public void listQuestion() throws IOException {
        List<AbstractQuestion> ql  = null;
        Message m = null;

        try{
            // creates a new object for accessing the database and lists all the questions
            ql = new ListQuestionDatabase(con).listQuestion();

            if(ql != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                new ResourceList(ql).toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new Message("Cannot list questions: unexpected error.", "E5A1", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            m = new Message("Cannot search list: unexpected error.", "E5A1", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }

    /**
     * This method provide all the questions with the specified category
     * @throws IOException if any error occurs in the client/server communication.
     * @param category the id of the category.
     */
    public void listCategoryQuestion(int category) throws IOException{
        List<AbstractQuestion> ql  = null;
        Message m = null;

        try{

            // creates a new object for accessing the database and lists all the questions
            ql = new ListQuestionByCategoryDatabase(con, category).listCategoryQuestion();

            if(ql != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                new ResourceList(ql).toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new Message("Cannot list questions: unexpected error.", "E5A1", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            m = new Message("Cannot show question by ID: unexpected error.", "E5A1", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }

    /**
     * Reads an interview from the database.
     *
     * @throws IOException if any error occurs in the client/server communication.
     * @throws ServletException if any problem occurs while executing the servlet.
     *
     */
    public void readQuestion() throws ServletException, IOException {
        AbstractQuestion q = null;
        Message m = null;

        try{
            // parse the URI path to extract the id
            String path = req.getRequestURI();
            path = path.substring(path.lastIndexOf("question") + 8);
            final int id = Integer.parseInt(path.substring(1));

            // creates a new object for accessing the database and reads the user
            q= new ReadQuestionDatabase(con, id).readQuestion();

            if(q!= null) {
                res.setStatus(HttpServletResponse.SC_OK);
                q.toJSON(res.getOutputStream());
            } else {
                m = new Message(String.format("AbstractQuestion %d not found.", id), "E5A3", null);
                res.setStatus(HttpServletResponse.SC_NOT_FOUND);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            m = new Message("Cannot read question: unexpected error.", "E5A1", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }

    }

    /**
     * Deletes a question from the database.
     *
     * @throws IOException if any error occurs in the client/server communication.
     */
    public void deleteQuestion() throws  IOException{

        AbstractQuestion q = null;
        Message m = null;

        try{
            // parse the URI path to extract the id
            String path = req.getRequestURI();
            path = path.substring(path.lastIndexOf("question") + 8);
            final int id = Integer.parseInt(path.substring(1));

            // creates a new object for accessing the database and reads the user
            q= new DeleteQuestionDataBase(con, id).deleteQuestion();

            if(q!= null) {
                res.setStatus(HttpServletResponse.SC_OK);
                q.toJSON(res.getOutputStream());
            } else {
                m = new Message(String.format("AbstractQuestion %d not found.", id), "E5A3", null);
                res.setStatus(HttpServletResponse.SC_NOT_FOUND);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            m = new Message("Cannot delete question: unexpected error.", "E5A1", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }

    }
}


