package it.unipd.dei.webapp.rest;

import it.unipd.dei.webapp.database.ListInterviewDatabase;
import it.unipd.dei.webapp.database.ListQuestionByInterviewDatabase;
import it.unipd.dei.webapp.resource.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.util.List;

/**
 * Manages the REST API for the {@link AbstractInterview} resource.
 *
 * @author Apollo group
 * @version 1.00
 * @since 1.00
 */
public class InterviewRestResource extends RestResource{

    /**
     * Creates a new REST resource for managing {@code InterviewIntCompany} resources.
     *
     * @param req the HTTP request.
     * @param res the HTTP response.
     * @param con the connection to the database.
     */
    public InterviewRestResource(final HttpServletRequest req, final HttpServletResponse res, Connection con) {
        super(req, res, con);
    }

    /**
     * This method return all the interviews saved into the database
     * @throws IOException if any error occurs in the client/server communication.
     */
    public void listInterview() throws IOException {

        List<InterviewStringCompany> il  = null;
        Message m = null;

        try{
            // creates a new object for accessing the database and lists all the interviews
            il = new ListInterviewDatabase(con).listInterview();

            if(il != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                new ResourceList(il).toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new Message("Cannot list interview: unexpected error.", "E5A1", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t)
        {
            m = new Message("Cannot search list: unexpected error.", "E5A1", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }

    /**
     * This method return all the question saved into the database for the given interview
     * @throws IOException if any error occurs in the client/server communication.
     * @param id the identifier of the interview.
     */

    public void ListQuestionByInterview(int id) throws IOException {

        List<AbstractQuestion> ql  = null;
        Message m = null;

        try{
            // creates a new object for accessing the database and lists all the interviews
            ql = new ListQuestionByInterviewDatabase(con, id).readQuestion();

            if(ql != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                new ResourceList(ql).toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new Message("Cannot list interview: unexpected error.", "E5A1", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t)
        {
            m = new Message("Cannot search list: unexpected error.", "E5A1", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }
}


