package it.unipd.dei.webapp.rest;

import it.unipd.dei.webapp.database.*;
import it.unipd.dei.webapp.resource.Answer;
import it.unipd.dei.webapp.resource.Message;
import it.unipd.dei.webapp.resource.ResourceList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Manages the REST API for the {@link Answer} resource.
 *
 * @author Apollo group
 * @version 1.00
 * @since 1.00
 */
public class AnswerRestResource extends RestResource
{
    /**
     * Creates a new REST resource for managing {@code Answer} resources.
     *
     * @param req the HTTP request.
     * @param res the HTTP response.
     * @param con the connection to the database.
     */
    public AnswerRestResource(final HttpServletRequest req, final HttpServletResponse res, Connection con) {
        super(req, res, con);
    }

    /**
     * Create new answer into the database
     * @throws IOException if any error occurs in the client/server communication.
     */
    public void createAnswer() throws IOException {

        Answer a = null;
        Message m = null;

        try{
            final Answer answer  = Answer.fromJSON(req.getInputStream());

            // creates a new object for accessing the database and stores the answer
            a = new CreateAnswerDatabase(con, answer).createAnswer();

            if(a != null) {
                res.setStatus(HttpServletResponse.SC_CREATED);
                a.toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new Message("Cannot create the answer: unexpected error.", "E5A1", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }catch (Throwable t){
            if (t instanceof SQLException && ((SQLException) t).getSQLState().equals("23505")) {
                m = new Message("Cannot create the answer: it already exists.", "E5A2", t.getMessage());
                res.setStatus(HttpServletResponse.SC_CONFLICT);
                m.toJSON(res.getOutputStream());
            } else {
                m = new Message("Cannot create the answer: unexpected error.", "E5A1", t.getMessage());
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        }
    }

    /**
     * Reads an answer from the database.
     *
     * @throws IOException
     *             if any error occurs in the client/server communication.
     */
    public void readAnswer() throws IOException {
        Answer a = null;
        Message m = null;

        try{
            // parse the URI path to extract the id
            String path = req.getRequestURI();
            path = path.substring(path.lastIndexOf("answer") + 6);
            final int id = Integer.parseInt(path.substring(1));

            // creates a new object for accessing the database and read the answer
            a = new ReadAnswerDatabase(con, id).readAnswer();

            if(a!= null) {
                res.setStatus(HttpServletResponse.SC_OK);
                a.toJSON(res.getOutputStream());
            } else {
                m = new Message(String.format("Answer %d not found.", id), "E5A3", null);
                res.setStatus(HttpServletResponse.SC_NOT_FOUND);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            m = new Message("Cannot read answer: unexpected error.", "E5A1", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }

    }

    /**
     * This method provide all the answer for the specified question
     * @param question integer indicating the question id
     * @throws IOException if any error occurs in the client/server communication.
     */
    public void listAnswerByQuestion(int question) throws IOException{
        List<Answer> al  = null;
        Message m = null;

        try{

            // creates a new object for accessing the database and lists all the questions
            al = new ListAnswerByQuestionDatabase(con, question).listAnswerByQuestion();

            if(al != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                new ResourceList(al).toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new Message("Cannot list answers: unexpected error.", "E5A1", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            m = new Message("Cannot show answers by question ID: unexpected error.", "E5A1", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }

    /**
     * This method provide all the answer for the specified user
     * @param userEmail string indicating the user id
     * @throws IOException if any error occurs in the client/server communication.
     */
    public void listAnswerByUser(String userEmail) throws IOException{
        List<Answer> al  = null;
        Message m = null;

        try{

            // creates a new object for accessing the database and lists all the questions
            al = new ListAnswerByUserDatabase(con, userEmail).listAnswerByUser();

            if(al != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                new ResourceList(al).toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new Message("Cannot list answers: unexpected error.", "E5A1", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            m = new Message("Cannot show answers by userEmail: unexpected error.", "E5A1", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }
    }

    /**
     * This method delete an answer
     * @throws IOException if any error occurs in the client/server communication.
     */
    public void deleteAnswer() throws  IOException{

        Answer a = null;
        Message m = null;

        try{
            // parse the URI path to extract the id
            String path = req.getRequestURI();
            path = path.substring(path.lastIndexOf("answer") + 6);
            final int id = Integer.parseInt(path.substring(1));

            // creates a new object for accessing the database and reads the user
            a = new DeleteAnswerDatabase(con, id).deleteAnswer();

            if(a!= null) {
                res.setStatus(HttpServletResponse.SC_OK);
                a.toJSON(res.getOutputStream());
            } else {
                m = new Message(String.format("Answer %d not found.", id), "E5A3", null);
                res.setStatus(HttpServletResponse.SC_NOT_FOUND);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            m = new Message("Cannot delete answer: unexpected error.", "E5A1", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }

    }
}
