package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Answer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Read an answer from the database.
 *
 * @author Apollo group
 * @version 1.00
 * @since 1.00
 */
public class ReadAnswerDatabase
{
    /**
     * The SQL statement to be executed.
     */
    private static final String STATEMENT = "SELECT id, userEmail, question, text, date FROM apollo.answer WHERE id = ?";


    /**
     * The connection to the database.
     */
    private final Connection con;

    /**
     * The id of the answer.
     */
    private final int id;

    /**
     * Creates a new object for read a specific answer.
     *
     * @param con the connection to the database.
     * @param id the id of the answer to be read.
     */
    public ReadAnswerDatabase(final Connection con, int id) {
        this.con = con;
        this.id = id;
    }

    /**
     * Return the answer in the database.
     *
     * @return a {@code Answer} object.
     * @throws SQLException if any error occurs while searching for questions.
     */
    public Answer readAnswer() throws SQLException {

        PreparedStatement pstmt = null;

        ResultSet rs = null;

        // the results of the search
        Answer answer = null;

        try {
            pstmt = con.prepareStatement(STATEMENT);
            pstmt.setInt(1, id);

            rs = pstmt.executeQuery();

            while (rs.next()) {
                answer = new Answer(rs.getInt("id"),
                        rs.getString("userEmail"),
                        rs.getInt("question"),
                        rs.getString("text"),
                        rs.getDate("date")
                        );
            }
        } finally {
            if (rs != null) {
                rs.close();
            }

            if (pstmt != null) {
                pstmt.close();
            }

            con.close();
        }

        return answer;
    }
}
