package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Answer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * List the answers in the database for a given question id.
 *
 * @author Apollo group
 * @version 1.00
 * @since 1.00
 *
 */
public class ListAnswerByQuestionDatabase
{
    /**
     * The SQL statement to be executed
     */
    private static final String STATEMENT = "SELECT id, userEmail, question, text, date FROM apollo.answer WHERE question=?";


    /**
     * question ID
     */
    private final int question;

    /**
     * The connection to the database
     */
    private final Connection con;

    /**
     * Creates a new object for listing all the answers for the given question.
     *
     * @param con the connection to the database.
     * @param question the question ID
     */
    public ListAnswerByQuestionDatabase(final Connection con, final int question) {
        this.con = con;
        this.question = question;
    }


    /**
     * Lists all the answers in the database for the given question id.
     *
     * @return a list of {@code AbstractQuestion} object.
     * @throws SQLException if any error occurs while searching for questions.
     */
    public List<Answer> listAnswerByQuestion() throws SQLException {

        PreparedStatement pstmt = null;
        ResultSet rs = null;


        // the results of the search
        final List<Answer> answers = new ArrayList<Answer>();

        try {
            pstmt = con.prepareStatement(STATEMENT);
            pstmt.setInt(1, question);

            rs = pstmt.executeQuery();

            while (rs.next()) {
                answers.add(new Answer(rs.getInt("id"),
                        rs.getString("userEmail"),
                        rs.getInt("question"),
                        rs.getString("text"),
                        rs.getDate("date")
                        ));
            }
        } finally {
            if (rs != null) {
                rs.close();
            }

            if (pstmt != null) {
                pstmt.close();
            }

            con.close();
        }

        return answers;
    }
}
