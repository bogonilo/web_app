package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Category;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Return the list of all category into the database.
 *
 * @author Apollo group
 * @version 1.00
 * @since 1.00
 */
public class ListCategoryDatabase
{

    /**
     * The SQL statement to be executed
     */
    private static final String STATEMENT = "SELECT id, name FROM apollo.category";


    /**
     * The connection to the database
     */
    private final Connection con;

    /**
     * Creates a new object for listing all the question.
     *
     * @param con the connection to the database.
     */
    public ListCategoryDatabase(final Connection con) {
        this.con = con;
    }

    /**
     * Lists all the category in the database.
     *
     * @return a list of {@code Category} object.
     * @throws SQLException if any error occurs while searching for questions.
     */
    public List<Category> listCategory() throws SQLException {

        PreparedStatement pstmt = null;

        ResultSet rs = null;

        // the results of the search
        final List<Category> categories = new ArrayList<Category>();

        try {
            pstmt = con.prepareStatement(STATEMENT);

            rs = pstmt.executeQuery();

            while (rs.next()) {
                categories.add(new Category(rs.getInt("id"), rs.getString("name")));
            }
        } finally {
            if (rs != null) {
                rs.close();
            }

            if (pstmt != null) {
                pstmt.close();
            }

            con.close();
        }

        return categories;
    }

}
