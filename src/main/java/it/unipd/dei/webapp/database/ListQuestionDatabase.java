package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.AbstractQuestion;
import it.unipd.dei.webapp.resource.QuestionStringInterview;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Return the list of all question into the database.
 *
 * @author Apollo group
 * @version 1.00
 * @since 1.00
 */
public class ListQuestionDatabase {

    /**
     * The SQL statement to be executed
     */
    private static final String STATEMENT = "SELECT Q.id AS id, Q.category AS category, Q.title AS title, Q.text AS text, I.id AS interviewId, I.title AS interviewTitle, Q.userEmail AS userEmail " +
                                            "FROM apollo.question AS Q " +
                                            "INNER JOIN apollo.interview AS I ON Q.interview = I.id " +
                                            "ORDER BY Q.id DESC";

    /**
     * The connection to the database
     */
    private final Connection con;

    /**
     * Creates a new object for listing all the question.
     *
     * @param con the connection to the database.
     */
    public ListQuestionDatabase(final Connection con) {
        this.con = con;
    }

    /**
     * Lists all the questions in the database.
     *
     * @return a list of {@code AbstractQuestion} object.
     * @throws SQLException if any error occurs while searching for questions.
     */
    public List<AbstractQuestion> listQuestion() throws SQLException {

        PreparedStatement pstmt = null;

        ResultSet rs = null;

        // the results of the search
        final List<AbstractQuestion> questions = new ArrayList<AbstractQuestion>();

        try {
            pstmt = con.prepareStatement(STATEMENT);

            rs = pstmt.executeQuery();

            while (rs.next()) {
                questions.add(new QuestionStringInterview(rs.getInt("id"), rs.getInt("category"), rs.getString("title"), rs.getString("text"), rs.getInt("interviewId"), rs.getString("interviewTitle"), rs.getString("userEmail")));
            }
        } finally {
            if (rs != null) {
                rs.close();
            }

            if (pstmt != null) {
                pstmt.close();
            }

            con.close();
        }

        return questions;
    }

}
