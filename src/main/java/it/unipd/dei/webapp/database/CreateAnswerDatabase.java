package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Answer;

import java.sql.*;

/**
 * Creates a new Answer in the database.
 *
 * @author Apollo group
 * @version 1.00
 * @since 1.00
 */
public final class CreateAnswerDatabase
{
    /**
     * The SQL statement to be executed
     */
    private static final String STATEMENT = "INSERT INTO apollo.answer (userEmail, question, text, date) VALUES (?, ?, ?, ?) RETURNING *";

    /**
     * The connection to the database
     */
    Connection con;

    /**
     * The Answer to be added in the database
     */
    Answer answer;

    /**
     * Creates a new object for adding a new answer
     *
     * @param con
     *            the connection to the database.
     * @param answer
     *            the answer to be created in the database.
     */
    public CreateAnswerDatabase(final Connection con, final Answer answer) {
        this.con = con;
        this.answer = answer;
    }

    /**
     * Creates an Answer in the database.
     *
     * @return the {@code Answer} object matching the id.
     *
     * @throws SQLException
     *             if any error occurs while reading the userEmail.
     */
    public Answer createAnswer() throws SQLException {

        PreparedStatement pstmt = null;
        ResultSet rs = null;

        // the create answer
        Answer q = null;

        try {
            pstmt = con.prepareStatement(STATEMENT);
            pstmt.setString(1, answer.getUser());
            pstmt.setInt(2, answer.getQuestion());
            pstmt.setString(3, answer.getText());
            pstmt.setDate(4, (Date) answer.getDate());


            rs = pstmt.executeQuery();


            if (rs.next()) {
                q = new Answer(
                        rs.getInt("id"),
                        rs.getString("userEmail"),
                        rs.getInt("question"),
                        rs.getString("text"),
                        rs.getDate("date"));
            }
        } finally {
            if (rs != null) {
                rs.close();
            }

            if (pstmt != null) {
                pstmt.close();
            }

            con.close();
        }

        return q;
    }
}
