package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.InterviewIntCompany;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CreateInterviewDatabase {

    /**
     * The SQL statement to be executed
     */
    private static final String STATEMENT = "INSERT INTO apollo.interview (company, date, title) VALUES (?, ?, ?) RETURNING *";

    /**
     * The connection to the database
     */
    private final Connection con;

    /**
     * The interview to be updated in the database
     */
    private final InterviewIntCompany interview;

    /**
     * Creates a new object for update an Interview.
     *
     * @param con
     *            the connection to the database.
     * @param interview
     *            the interview to be created in the database.
     */
    public CreateInterviewDatabase(final Connection con, final InterviewIntCompany interview) {
        this.con = con;
        this.interview = interview;
    }

    /**
     * Creates an interview in the database.
     *
     * @return the {@code InterviewIntCompany}.
     *
     * @throws SQLException
     *             if any error occurs while reading the user.
     */
    public InterviewIntCompany createInterview() throws SQLException {

        PreparedStatement pstmt = null;
        ResultSet rs = null;

        // the create interview
        InterviewIntCompany i = null;

        try {
            //todo add controllo se la company esiste
            pstmt = con.prepareStatement(STATEMENT);
            pstmt.setInt(1, interview.getCompany());
            pstmt.setDate(2, interview.getDate());
            pstmt.setString(3, interview.getTitle());

            rs = pstmt.executeQuery();

            //todo aggiungere controlli
			/*
				Prima di usare rs bisogna fare il controllo che sia diverso da null e poi bisogna
				creare anche il catch per stampare il messaggio di errore della query SQL
			 */

            if (rs.next()) {

                i = new InterviewIntCompany(rs.getInt("id"),
                        rs.getInt("company"),
                        rs.getDate("date"),
                        rs.getString("title"));
            }
        } finally {
            if (rs != null) {
                rs.close();
            }

            if (pstmt != null) {
                pstmt.close();
            }

            con.close();
        }

        return i;
    }
}
