/*
 * Copyright 2018 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Creates an user in the database.
 * 
 * @author Apollo group
 * @version 1.00
 * @since 1.00
 */
public final class CreateUserDatabase {

	/**
	 * The SQL statement to be executed
	 */
	private static final String STATEMENT = "INSERT INTO apollo.user_account (email, username, password, birthday, role) VALUES (?, ?, ?, ?, ?) RETURNING *";

	/**
	 * The connection to the database
	 */
	private final Connection con;

	/**
	 * The user to be updated in the database
	 */
	private final User user;

	/**
	 * Creates a new object for update an user.
	 *
	 * @param con
	 *            the connection to the database.
	 * @param user
	 *            the user to be created in the database.
	 */
	public CreateUserDatabase(final Connection con, final User user) {
		this.con = con;
		this.user = user;
	}

	/**
	 * Creates an user in the database.
	 * 
	 * @return the {@code User} object matching the email.
	 * 
	 * @throws SQLException
	 *             if any error occurs while reading the user.
	 */
	public User createUser() throws SQLException {

		PreparedStatement pstmt = null;
		ResultSet rs = null;

		// the create user
		User u = null;

		try {
			pstmt = con.prepareStatement(STATEMENT);
			pstmt.setString(1, user.getEmail());
			pstmt.setString(2, user.getUsername());
			pstmt.setString(3, user.getPassword());
			pstmt.setDate(4, user.getBirthday());
			pstmt.setString(5, user.getRole().toString());

			rs = pstmt.executeQuery();

			// TODO: 19/05/18
			/*
				Prima di usare rs bisogna fare il controllo che sia diverso da null e poi bisogna
				creare anche il catch per stampare il messaggio di errore della query SQL
			 */

			if (rs.next()) {
				User.ROLE role = null;
				switch (rs.getString("role")) {
					case "ADMIN":
						role = User.ROLE.ADMIN; break;
					case "USER":
						role = User.ROLE.USER; break;
					case "admin":
						role = User.ROLE.ADMIN; break;
					case "user":
						role = User.ROLE.USER; break;
				}
				u = new User(rs.getString("email"),
							rs.getString("username"),
							rs.getString("password"),
							rs.getDate("birthday"),
							role);
			}
		} finally {
			if (rs != null) {
				rs.close();
			}

			if (pstmt != null) {
				pstmt.close();
			}

			con.close();
		}

		return u;
	}
}
