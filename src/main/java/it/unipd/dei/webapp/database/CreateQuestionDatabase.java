/*
 * Copyright 2018 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.AbstractQuestion;
import it.unipd.dei.webapp.resource.QuestionIntInterview;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Creates a new AbstractQuestion in the database.
 * 
 * @author Apollo group
 * @version 1.00
 * @since 1.00
 */
public final class CreateQuestionDatabase {

	/**
	 * The SQL statement to be executed
	 */
	private static final String STATEMENT = "INSERT INTO apollo.question (category, title, text, interview, userEmail) VALUES (?, ?, ?, ?, ?) RETURNING *";

	/**
	 * The connection to the database
	 */
	private Connection con;

	/**
	 * The userEmail to be updated in the database
	 */
	private QuestionIntInterview question;

	/**
	 * Creates a new object for updat an userEmail.
	 *
	 * @param con
	 *            the connection to the database.
	 * @param question
	 *            the question to be created in the database.
	 */
	public CreateQuestionDatabase(final Connection con, final QuestionIntInterview question) {
		this.con = con;
		this.question = question;
	}

	/**
	 * Creates an AbstractQuestion in the database.
	 * 
	 * @return the {@code AbstractQuestion} object matching the id.
	 * 
	 * @throws SQLException
	 *             if any error occurs while reading the userEmail.
	 */
	public AbstractQuestion createQuestion() throws SQLException {

		PreparedStatement pstmt = null;
		ResultSet rs = null;

		// the create userEmail
		AbstractQuestion q = null;

		try {
			pstmt = con.prepareStatement(STATEMENT);
			pstmt.setInt(1, question.getCategory());
			pstmt.setString(2, question.getTitle());
			pstmt.setString(3, question.getText());
			pstmt.setInt(4, question.getInterview());
			pstmt.setString(5, question.getUserEmail());


			rs = pstmt.executeQuery();


			if (rs.next()) {
				q = new QuestionIntInterview(
							rs.getInt("id"),
							rs.getInt("category"),
							rs.getString("title"),
							rs.getString("text"),
							rs.getInt("interview"),
							rs.getString("userEmail"));
			}
		} finally {
			if (rs != null) {
				rs.close();
			}

			if (pstmt != null) {
				pstmt.close();
			}

			con.close();
		}

		return q;
	}
}
