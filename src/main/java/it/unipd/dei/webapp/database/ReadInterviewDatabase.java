package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.InterviewIntCompany;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Read an interview from the database.
 *
 * @author Apollo group
 * @version 1.00
 * @since 1.00
 */
public class ReadInterviewDatabase
{
    /**
     * The SQL statement to be executed.
     */
    private static final String STATEMENT = "SELECT id, company, date, title FROM apollo.interview WHERE id = ?";


    /**
     * The connection to the database.
     */
    private final Connection con;

    /**
     * The id of the interview.
     */
    private final int id;

    /**
     * Creates a new object for read a specific interview.
     *
     * @param con the connection to the database.
     * @param id the identifier of the interview.
     */
    public ReadInterviewDatabase(final Connection con, int id) {
        this.con = con;
        this.id = id;
    }

    /**
     * Return the interview in the database.
     *
     * @return a {@code Interview} object.
     * @throws SQLException if any error occurs while searching for interviews.
     */
    public InterviewIntCompany readInterview() throws SQLException {

        PreparedStatement pstmt = null;

        ResultSet rs = null;

        // the results of the search
        InterviewIntCompany interview = null;

        try {
            pstmt = con.prepareStatement(STATEMENT);
            pstmt.setInt(1, id);

            rs = pstmt.executeQuery();

            while (rs.next()) {
                interview = new InterviewIntCompany(rs.getInt("id"), rs.getInt("company"), rs.getDate("date"), rs.getString("title"));
            }
        } finally {
            if (rs != null) {
                rs.close();
            }

            if (pstmt != null) {
                pstmt.close();
            }

            con.close();
        }

        return interview;
    }
}
