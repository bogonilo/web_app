/*
 * Copyright 2018 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Deletes an user from the database.
 * 
 * @author Apollo group
 * @version 1.00
 * @since 1.00
 */
public final class DeleteUserDatabase {

	/**
	 * The SQL statement to be executed
	 */
	private static final String STATEMENT = "DELETE FROM apollo.user_account WHERE email = ? RETURNING *";

	/**
	 * The connection to the database
	 */
	private final Connection con;

	/**
	 * The email of the user
	 */
	private final String email;

	/**
	 * Creates a new object for deleting an user.
	 *
	 * @param con
	 *            the connection to the database.
	 * @param email
	 *            the email of the user.
	 */
	public DeleteUserDatabase(final Connection con, final String email) {
		this.con = con;
		this.email = email;
	}

	/**
	 * Deletes an user from the database.
	 * 
	 * @return the {@code User} object matching the email.
	 * 
	 * @throws SQLException
	 *             if any error occurs while reading the user.
	 */
	public User deleteUser() throws SQLException {

		PreparedStatement pstmt = null;
		ResultSet rs = null;

		// the deleted user
		User u = null;

		try {
			pstmt = con.prepareStatement(STATEMENT);
			pstmt.setString(1, email);

			rs = pstmt.executeQuery();

			if (rs.next()) {
				User.ROLE role = null;
				switch (rs.getString("role")) {
					case "ADMIN":
						role = User.ROLE.ADMIN; break;
					case "USER":
						role = User.ROLE.USER; break;
					case "admin":
						role = User.ROLE.ADMIN; break;
					case "user":
						role = User.ROLE.USER; break;
				}
				u = new User(rs.getString("email"),
						rs.getString("username"),
						rs.getString("password"),
						rs.getDate("birthday"),
						role);
			}
		} finally {
			if (rs != null) {
				rs.close();
			}

			if (pstmt != null) {
				pstmt.close();
			}

			con.close();
		}

		return u;
	}
}
