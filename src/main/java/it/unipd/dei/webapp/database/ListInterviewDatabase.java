package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.InterviewStringCompany;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Return the list of all interview into the database.
 *
 * @author Apollo group
 * @version 1.00
 * @since 1.00
 */
public class ListInterviewDatabase
{

    /**
     * The SQL statement to be executed
     */
    private static final String STATEMENT = "SELECT I.id AS id, C.name AS company, I.date AS date, I.title as title " +
                                            "FROM apollo.interview AS I " +
                                            "INNER JOIN apollo.company AS C ON I.company = C.id " +
                                            "ORDER BY I.date DESC";

    /**
     * The connection to the database
     */
    private final Connection con;

    /**
     * Creates a new object for listing all the interview.
     *
     * @param con the connection to the database.
     */
    public ListInterviewDatabase(final Connection con) {
        this.con = con;
    }

    /**
     * Lists all the interviews in the database.
     *
     * @return a list of {@code Interview} object.
     * @throws SQLException if any error occurs while searching for interviews.
     */
    public List<InterviewStringCompany> listInterview() throws SQLException {

        PreparedStatement pstmt = null;

        ResultSet rs = null;

        // the results of the search
        final List<InterviewStringCompany> interviews = new ArrayList<InterviewStringCompany>();

        try {
            pstmt = con.prepareStatement(STATEMENT);

            rs = pstmt.executeQuery();

            while (rs.next()) {
                interviews.add(new InterviewStringCompany(rs.getInt("id"), rs.getString("company"), rs.getDate("date"), rs.getString("title")));
            }
        } finally {
            if (rs != null) {
                rs.close();
            }

            if (pstmt != null) {
                pstmt.close();
            }

            con.close();
        }

        return interviews;
    }

}
