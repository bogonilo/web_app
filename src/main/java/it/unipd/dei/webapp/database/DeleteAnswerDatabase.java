package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Answer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Delete an Answer from the database.
 *
 * @author Apollo group
 * @version 1.00
 * @since 1.00
 */
public class DeleteAnswerDatabase
{

    /**
     * The SQL statement to be executed.
     */
    private static final String STATEMENT = "DELETE FROM apollo.answer WHERE id = ?";


    /**
     * The connection to the database.
     */
    private final Connection con;

    /**
     * The id of the answer.
     */
    private final int id;

    /**
     * Delete a specific answer.
     *
     * @param con the connection to the database.
     * @param id the identifier of the answer.
     */
    public DeleteAnswerDatabase(final Connection con, int id) {
        this.con = con;
        this.id = id;
    }

    /**
     * Return the answer deleted from the database.
     *
     * @return a {@code Answer} object.
     * @throws SQLException if any error occurs while searching for questions.
     */
    public Answer deleteAnswer() throws SQLException {

        PreparedStatement pstmt = null;

        ResultSet rs = null;

        // the results of the search
        Answer answer = null;

        try {
            pstmt = con.prepareStatement(STATEMENT);
            pstmt.setInt(1,id);

            rs = pstmt.executeQuery();

            while (rs.next()) {
                answer = new Answer(rs.getInt("id"),
                        rs.getString("userEmail"),
                        rs.getInt("question"),
                        rs.getString("text"),
                        rs.getDate("date")
                        );
            }
        } finally {
            if (rs != null) {
                rs.close();
            }

            if (pstmt != null) {
                pstmt.close();
            }

            con.close();
        }

        return answer;
    }
}
