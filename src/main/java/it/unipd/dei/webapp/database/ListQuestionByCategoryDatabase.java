package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.AbstractQuestion;
import it.unipd.dei.webapp.resource.QuestionStringInterview;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Return the list of all question into the database with a category id specified.
 *
 * @author Apollo group
 * @version 1.00
 * @since 1.00
 */
public class ListQuestionByCategoryDatabase {

    /**
     * The SQL statement to be executed
     */
    private static final String STATEMENT = "SELECT Q.id AS id, Q.category AS category, Q.title AS title, Q.text AS text, I.id AS interviewId, I.title AS interviewTitle, Q.userEmail AS userEmail " +
                                            "FROM apollo.question AS Q " +
                                            "INNER JOIN apollo.interview AS I ON Q.interview = I.id " +
                                            "WHERE category=? " +
                                            "ORDER BY Q.id DESC";

    /**
     * category ID
     */
    private final int category;

    /**
     * The connection to the database
     */
    private final Connection con;

    /**
     * Creates a new object for listing all the question with category == category.
     *
     * @param con the connection to the database.
     * @param category the identifier of the category.
     *
     */
    public ListQuestionByCategoryDatabase(final Connection con, final int category) {
        this.con = con;
        this.category = category;
    }


    /**
     * Lists all the questions in the database.
     *
     * @return a list of {@code AbstractQuestion} object.
     * @throws SQLException if any error occurs while searching for questions.
     */
    public List<AbstractQuestion> listCategoryQuestion() throws SQLException {

        PreparedStatement pstmt = null;
        ResultSet rs = null;


        // the results of the search
        final List<AbstractQuestion> questions = new ArrayList<AbstractQuestion>();

        try {
            pstmt = con.prepareStatement(STATEMENT);
            pstmt.setInt(1,category);

            rs = pstmt.executeQuery();

            while (rs.next()) {
                questions.add(new QuestionStringInterview(rs.getInt("id"), rs.getInt("category"), rs.getString("title"), rs.getString("text"), rs.getInt("interviewId"), rs.getString("interviewTitle"), rs.getString("userEmail")));
            }
        } finally {
            if (rs != null) {
                rs.close();
            }

            if (pstmt != null) {
                pstmt.close();
            }

            con.close();
        }

        return questions;
    }

}
