package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Answer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * List the answers in the database for a given userEmail.
 *
 * @author Apollo group
 * @version 1.00
 * @since 1.00
 *
 */
public class ListAnswerByUserDatabase
{
    /**
     * The SQL statement to be executed
     */
    private static final String STATEMENT = "SELECT id, userEmail, question, text, date FROM apollo.answer WHERE userEmail=?";


    /**
     * userEmail foreign key
     */
    private final String userEmail;

    /**
     * The connection to the database
     */
    private final Connection con;

    /**
     * Creates a new object for listing all the answer from a given user.
     *
     * @param con the connection to the database.
     * @param userEmail the userEmail of the user.
     */
    public ListAnswerByUserDatabase(final Connection con, final String userEmail) {
        this.con = con;
        this.userEmail = userEmail;
    }


    /**
     * Lists all the answers in the database for the userEmail.
     *
     * @return a list of {@code Answer} object.
     * @throws SQLException if any error occurs while searching for answers.
     */
    public List<Answer> listAnswerByUser() throws SQLException {

        PreparedStatement pstmt = null;
        ResultSet rs = null;


        // the results of the search
        final List<Answer> answers = new ArrayList<Answer>();

        try {
            pstmt = con.prepareStatement(STATEMENT);
            pstmt.setString(1, userEmail);

            rs = pstmt.executeQuery();

            while (rs.next()) {
                answers.add(new Answer(rs.getInt("id"),
                        rs.getString("userEmail"),
                        rs.getInt("question"),
                        rs.getString("text"),
                        rs.getDate("Date")
                ));
            }
        } finally {
            if (rs != null) {
                rs.close();
            }

            if (pstmt != null) {
                pstmt.close();
            }

            con.close();
        }

        return answers;
    }
}
