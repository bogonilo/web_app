package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.AbstractQuestion;
import it.unipd.dei.webapp.resource.QuestionIntInterview;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
/**
 * Read an question from the database.
 *
 * @author Apollo group
 * @version 1.00
 * @since 1.00
 */
public class ReadQuestionDatabase {
    /**
     * The SQL statement to be executed.
     */
    private static final String STATEMENT = "SELECT id, category, title, text, interview, userEmail FROM apollo.question WHERE id = ?";


    /**
     * The connection to the database.
     */
    private final Connection con;

    /**
     * The id of the question.
     */
    private final int id;

    /**
     * Creates a new object for read a specific question.
     *
     * @param con the connection to the database.
     * @param id the identifier of the question.
     */
    public ReadQuestionDatabase(final Connection con, int id) {
        this.con = con;
        this.id = id;
    }

    /**
     * Return the question in the database.
     *
     * @return a {@code AbstractQuestion} object.
     * @throws SQLException if any error occurs while searching for questions.
     */
    public AbstractQuestion readQuestion() throws SQLException {

        PreparedStatement pstmt = null;

        ResultSet rs = null;

        // the results of the search
        AbstractQuestion questions = null;

        try {
            pstmt = con.prepareStatement(STATEMENT);
            pstmt.setInt(1, id);

            rs = pstmt.executeQuery();

            while (rs.next()) {
                questions = new QuestionIntInterview(rs.getInt("id"), rs.getInt("category"), rs.getString("title"), rs.getString("text"), rs.getInt("interview"), rs.getString("userEmail"));
            }
        } finally {
            if (rs != null) {
                rs.close();
            }

            if (pstmt != null) {
                pstmt.close();
            }

            con.close();
        }

        return questions;
    }
}
