package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.AbstractQuestion;
import it.unipd.dei.webapp.resource.QuestionIntInterview;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Return the list of all question of a given interview into the database.
 *
 * @author Apollo group
 * @version 1.00
 * @since 1.00
 */
public class ListQuestionByInterviewDatabase
{

    /**
     * The SQL statement to be executed
     */
    private static final String STATEMENT =  "SELECT id, category, title, text, interview, userEmail FROM apollo.question WHERE interview = ?";

    /**
     * The connection to the database
     */
    private final Connection con;

    /**
     * The id of the interview.
     */
    private final int id;

    /**
     * Creates a new object for listing all the question.
     *
     * @param con the connection to the database.
     * @param id the id of the interview
     */
    public ListQuestionByInterviewDatabase(final Connection con, int id) {
        this.con = con;
        this.id = id;
    }

    /**
     * Lists all the questions in the database.
     *
     * @return a list of {@code AbstractQuestion} object.
     * @throws SQLException if any error occurs while searching for questions.
     */
    public List<AbstractQuestion> readQuestion() throws SQLException {

        PreparedStatement pstmt = null;

        ResultSet rs = null;

        // the results of the search
        final List<AbstractQuestion> questions = new ArrayList<AbstractQuestion>();

        try {
            pstmt = con.prepareStatement(STATEMENT);
            pstmt.setInt(1,id);

            rs = pstmt.executeQuery();

            while (rs.next()) {
                questions.add(new QuestionIntInterview(rs.getInt("id"), rs.getInt("category"), rs.getString("title"), rs.getString("text"), rs.getInt("interview"), rs.getString("userEmail")));
            }
        } finally {
            if (rs != null) {
                rs.close();
            }

            if (pstmt != null) {
                pstmt.close();
            }

            con.close();
        }

        return questions;
    }

}
