/*
 * Copyright 2018 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.resource;


import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;

/**
 * Represents the data about a comment.
 *
 * @author Apollo Group
 * @version 1.00
 * @since 1.00
 */
public class Comment extends AbstractResource
{

    /**
     * The id (identifier) of the comment
     */
    private final int id;
    /**
     * The {@code Answer} to the comment
     */
    private final int answer;
    /**
     * The email of the {@code User} that commented
     */
    private final String userEmail;
    /**
     * The text of the comment
     */
    private final String text;
    /**
     * The date of the comment
     */
    private final Date date;

    /**
     * Creates a new comment
     *
     * @param id        the id of the comment
     * @param answer    the {@code Answer} to the comment
     * @param userEmail the email of the {@code User} that commented
     * @param text      the text of the comment
     * @param date      the date of the comment
     */
    public Comment(int id, int answer, String userEmail, String text, Date date) {
        this.id = id;
        this.answer = answer;
        this.userEmail = userEmail;
        this.text = text;
        this.date = date;
    }

    /**
     * Returns the id of the comment.
     *
     * @return the id of the comment.
     */
    public final int getId() {
        return id;
    }

    /**
     * Returns the id of the {@code Answer}.
     *
     * @return the id of the {@code Answer}.
     */
    public final int getAnswer() {
        return answer;
    }

    /**
     * Returns the email of the {@code User} that commented.
     *
     * @return the email of the {@code User} that commented.
     */
    public final String getUserEmail() {
        return userEmail;
    }

    /**
     * Returns the text of the comment.
     *
     * @return the text of the comment.
     */
    public final String getText() {
        return text;
    }

    /**
     * Returns the date of the comment.
     *
     * @return the date of the comment.
     */
    public final Date getDate() {
        return date;
    }

    /**
     * Creates a JsonGenerator that generates the comment in the JSON format
     *
     * @param out the stream to which the JSON representation of the {@code AbstractResource} has to be written.
     * @throws IOException if something goes wrong while creating the generator.
     */
    @Override
    public final void toJSON(final OutputStream out) throws IOException {

        final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

        jg.writeStartObject();

        jg.writeFieldName("comment");

        jg.writeStartObject();

        jg.writeNumberField("id", id);

        jg.writeStringField("userEmail", userEmail);

        jg.writeNumberField("answer", answer);

        jg.writeStringField("text", text);

        jg.writeStringField("date", date.toString());

        jg.writeEndObject();

        jg.writeEndObject();

        jg.flush();
    }

    /**
     * Creates a {@code Comment} from its JSON representation.
     *
     * @param in the input stream containing the JSON document.
     * @return the {@code Comment} created from the JSON representation.
     * @throws IOException if something goes wrong while parsing.
     */
    public static Comment fromJSON(final InputStream in) throws IOException {

        // the fields read from JSON
        int jId = -1;
        String jUser = null;
        int jAnswerId = -1;
        String jText = null;
        Date jDate = null;

        final JsonParser jp = JSON_FACTORY.createParser(in);

        // while we are not on the start of an element or the element is not
        // a token element, advance to the next element (if any)
        while (jp.getCurrentToken() != JsonToken.FIELD_NAME || "comment".equals(jp.getCurrentName()) == false) {

            // there are no more events
            if (jp.nextToken() == null) {
                throw new IOException("Unable to parse JSON: no comment object found.");
            }
        }

        while (jp.nextToken() != JsonToken.END_OBJECT) {

            if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {

                switch (jp.getCurrentName()) {
                    case "id":
                        jp.nextToken();
                        jId = jp.getIntValue();
                        break;
                    case "userEmail":
                        jp.nextToken();
                        jUser = jp.getText();
                        break;
                    case "answer":
                        jp.nextToken();
                        jAnswerId = jp.getIntValue();
                        break;
                    case "text":
                        jp.nextToken();
                        jText = jp.getText();
                        break;
                    case "date":
                        jp.nextToken();
                        jDate = new Date(jp.getText());
                        break;
                }
            }
        }

        return new Comment(jId, jAnswerId, jUser, jText, jDate);
    }
}
