/*
 * Copyright 2018 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.resource;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Represents the data about a question.
 *
 * @author Apollo Group
 * @version 1.00
 * @since 1.00
 */
public abstract class AbstractQuestion extends AbstractResource
{
    /**
     * The id (identifier) of the question
     */
    protected final int id;

    /**
     * The id of the {@code Category} to which the question belongs
     */
    protected final int category;

    /**
     * The title of the question
     */
    protected final String title;

    /**
     * The text of the question
     */
    protected final String text;

    /**
     * The email of the {@code User} who asked the question
     */
    protected final String userEmail;

    /**
     * Creates a new question
     *
     * @param id        the id of the question
     * @param category  the id of the {@code Category} to which the question belongs
     * @param title     the title of the question
     * @param text      the text of the question
     * @param userEmail the email of the {@code User} who asked the question
     */
    public AbstractQuestion(int id, int category, String title, String text, String userEmail) {

        this.id = id;
        this.category = category;
        this.title = title;
        this.text = text;
        this.userEmail = userEmail;
    }

    /**
     * Returns the id of the question.
     *
     * @return the id of the question.
     */
    public final int getId() {
        return id;
    }

    /**
     * Returns the id of the {@code Category}.
     *
     * @return the id of the {@code Category}.
     */
    public final int getCategory() {
        return category;
    }

    /**
     * Returns the title of the question.
     *
     * @return the title of the question.
     */
    public final String getTitle() {
        return title;
    }

    /**
     * Returns the text of the question.
     *
     * @return the text of the question.
     */
    public final String getText() {
        return text;
    }

    /**
     * Returns the email of the {@code User}.
     *
     * @return the email of the {@code User}.
     */
    public final String getUserEmail() {
        return userEmail;
    }

    /**
     * Returns the {@code InterviewIntCompany} of the question.
     *
     * @return the {@code InterviewIntCompany} of the question.
     */
    public abstract Object getInterview();

    /**
     * Creates a JsonGenerator that generates the question in the JSON format
     *
     * @param out the stream to which the JSON representation of the {@code AbstractResource} has to be written.
     * @throws IOException if something goes wrong while creating the generator.
     */
    @Override
    public abstract void toJSON(final OutputStream out) throws IOException;
}
