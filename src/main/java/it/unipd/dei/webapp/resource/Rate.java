/*
 * Copyright 2018 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.resource;


import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Represents the data about a rate.
 *
 * @author Apollo Group
 * @version 1.00
 * @since 1.00
 */
public class Rate extends AbstractResource
{

    /**
     * The email of the user that rates
     */
    private final String userEmail;
    /**
     * The id of the {@code Answer} getting rated
     */
    private final int answer;
    /**
     * The value of the rating
     */
    private final int value;

    /**
     * Creates a new Rate
     *
     * @param userEmail the email of the user that rates
     * @param answer    the id of the {@code Answer} getting rated
     * @param value     the value of the rating
     */
    public Rate(String userEmail, int answer, int value) {
        this.userEmail = userEmail;
        this.answer = answer;
        this.value = value;
    }

    /**
     * Returns the email of the user.
     *
     * @return the email of the user.
     */
    public final String getUserEmail() {
        return userEmail;
    }

    /**
     * Returns the id of the {@code Answer}.
     *
     * @return the id of the {@code Answer}.
     */
    public final int getAnswer() {
        return answer;
    }

    /**
     * Returns the value of the rating.
     *
     * @return the value of the rating.
     */
    public final int getValue() {
        return value;
    }


    /**
     * Creates a JsonGenerator that generates the rate in the JSON format
     *
     * @param out the stream to which the JSON representation of the {@code AbstractResource} has to be written.
     * @throws IOException if something goes wrong while creating the generator.
     */
    @Override
    public final void toJSON(final OutputStream out) throws IOException {

        final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

        jg.writeStartObject();

        jg.writeFieldName("rate");

        jg.writeStartObject();

        jg.writeStringField("userEmail", userEmail);

        jg.writeNumberField("answer", answer);

        jg.writeNumberField("value", value);

        jg.writeEndObject();

        jg.writeEndObject();

        jg.flush();
    }

    /**
     * Creates a {@code Rate} from its JSON representation.
     *
     * @param in the input stream containing the JSON document.
     * @return the {@code Rate} created from the JSON representation.
     * @throws IOException if something goes wrong while parsing.
     */
    public static Rate fromJSON(final InputStream in) throws IOException {

        // the fields read from JSON
        String jUserEmail = null;
        int jAnswer = -1;
        int jValue = -1;

        final JsonParser jp = JSON_FACTORY.createParser(in);

        // while we are not on the start of an element or the element is not
        // a token element, advance to the next element (if any)
        while (jp.getCurrentToken() != JsonToken.FIELD_NAME || !"rate".equals(jp.getCurrentName())) {

            // there are no more events
            if (jp.nextToken() == null) {
                throw new IOException("Unable to parse JSON: no rate object found.");
            }
        }

        while (jp.nextToken() != JsonToken.END_OBJECT) {

            if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {

                switch (jp.getCurrentName()) {
                    case "userEmail":
                        jp.nextToken();
                        jUserEmail = jp.getText();
                        break;
                    case "answer":
                        jp.nextToken();
                        jAnswer = jp.getIntValue();
                        break;
                    case "value":
                        jp.nextToken();
                        jValue = jp.getIntValue();
                        break;
                }
            }
        }

        return new Rate(jUserEmail, jAnswer, jValue);
    }
}
