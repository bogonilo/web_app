/*
 * Copyright 2018 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.resource;


import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Represents the data about a category.
 * 
 * @author Apollo Group
 * @version 1.00
 * @since 1.00
 */
public class Category extends AbstractResource
{

	/**
	 * The id (identifier) of the category
	 */
	private final int id;
	/**
	 * The name of the category
	 */
	private final String name;

	/**
	 * Creates a new category
	 * @param id the id of the category
	 * @param name the name of the category
	 */
	public Category(int id, String name) {
		this.id = id;
		this.name = name;
	}

	/**
	 * Returns the id of the category.
	 *
	 * @return the id of the category.
	 */
	public final int getId() {

		return id;
	}

	/**
	 * Returns the name of the category.
	 *
	 * @return the name of the category.
	 */
	public final String getName() {
		return name;
	}

	/**
	 * Creates a JsonGenerator that generates the category in the JSON format
	 *
	 * @param out the stream to which the JSON representation of the {@code AbstractResource} has to be written.
	 * @throws IOException if something goes wrong while creating the generator.
	 */
	@Override
	public final void toJSON(final OutputStream out) throws IOException {

		final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

		jg.writeStartObject();

		jg.writeFieldName("category");

		jg.writeStartObject();

		jg.writeNumberField("id", id);

		jg.writeStringField("name", name);

		jg.writeEndObject();

		jg.writeEndObject();

		jg.flush();
	}

	/**
	 * Creates a {@code Category} from its JSON representation.
	 *
	 * @param in the input stream containing the JSON document.
	 *
	 * @return the {@code Category} created from the JSON representation.
	 *
	 * @throws IOException if something goes wrong while parsing.
	 */
	public static Category fromJSON(final InputStream in) throws IOException {

		// the fields read from JSON
		int jId = -1;
		String jName = null;

		final JsonParser jp = JSON_FACTORY.createParser(in);

		// while we are not on the start of an element or the element is not
		// a token element, advance to the next element (if any)
		while (jp.getCurrentToken() != JsonToken.FIELD_NAME || "category".equals(jp.getCurrentName()) == false) {

			// there are no more events
			if (jp.nextToken() == null) {
				throw new IOException("Unable to parse JSON: no category object found.");
			}
		}

		while (jp.nextToken() != JsonToken.END_OBJECT) {

			if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {

				switch (jp.getCurrentName()) {
					case "id":
						jp.nextToken();
						jId = jp.getIntValue();
						break;
					case "name":
						jp.nextToken();
						jName = jp.getText();
						break;
				}
			}
		}

		return new Category(jId, jName);
	}
}
