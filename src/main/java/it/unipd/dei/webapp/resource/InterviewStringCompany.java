/*
 * Copyright 2018 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.resource;


import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Date;

/**
 * Represents the data about an interview.
 *
 * @author Apollo Group
 * @version 1.00
 * @since 1.00
 */
public class InterviewStringCompany extends AbstractInterview
{
    /**
     * The company of the interview
     */
    private final String company;

    /**
     * Creates a new interview
     *
     * @param id      the id of the interview
     * @param company the company of the interview
     * @param date    the date of the interview
     * @param title   the title of the interview
     */
    public InterviewStringCompany(int id, String company, Date date, String title) {
        super(id, date, title);
        this.company = company;
    }

    /**
     * Returns the company of the interview.
     *
     * @return the company of the interview.
     */
    public final String getCompany() {
        return company;
    }

    /**
     * Creates a JsonGenerator that generates the interview in the JSON format
     *
     * @param out the stream to which the JSON representation of the {@code AbstractResource} has to be written.
     * @throws IOException if something goes wrong while creating the generator.
     */
    @Override
    public final void toJSON(final OutputStream out) throws IOException {

        final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

        jg.writeStartObject();

        jg.writeFieldName("interview");

        jg.writeStartObject();

        jg.writeNumberField("id", super.id);

        jg.writeStringField("company", this.company);

        jg.writeStringField("date", super.date.toString());

        jg.writeStringField("title", super.title);

        jg.writeEndObject();

        jg.writeEndObject();

        jg.flush();
    }

    /**
     * Creates a {@code InterviewStringCompany} from its JSON representation.
     *
     * @param in the input stream containing the JSON document.
     * @return the {@code InterviewStringCompany} created from the JSON representation.
     * @throws IOException if something goes wrong while parsing.
     */
    public static InterviewStringCompany fromJSON(final InputStream in) throws IOException {

        // the fields read from JSON
        int jId = -1;
        String jCompanyId = null;
        Date jDate = null;
        String jTitle = null;

        final JsonParser jp = JSON_FACTORY.createParser(in);

        // while we are not on the start of an element or the element is not
        // a token element, advance to the next element (if any)
        while (jp.getCurrentToken() != JsonToken.FIELD_NAME || !"interview".equals(jp.getCurrentName())) {

            // there are no more events
            if (jp.nextToken() == null) {
                throw new IOException("Unable to parse JSON: no interview object found.");
            }
        }

        while (jp.nextToken() != JsonToken.END_OBJECT) {

            if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {

                switch (jp.getCurrentName()) {
                    case "id":
                        jp.nextToken();
                        jId = jp.getIntValue();
                        break;
                    case "company":
                        jp.nextToken();
                        jCompanyId = jp.getText();
                        break;
                    case "date":
                        jp.nextToken();
                        jDate = new Date(jp.getLongValue());
                        break;

                    case "title":
                        jp.nextToken();
                        jTitle = jp.getText();
                        break;
                }
            }
        }

        return new InterviewStringCompany(jId, jCompanyId, jDate, jTitle);
    }
}
