/*
 * Copyright 2018 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.resource;


import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Represents the data about an reference between a question and a topic.
 *
 * @author Apollo Group
 * @version 1.00
 * @since 1.00
 */
public class Refer extends AbstractResource
{

    /**
     * The id of the {@code AbstractQuestion}
     */
    private final int question;

    /**
     * The id of the {@code Topic}
     */
    private final int topic;

    /**
     * Creates a new reference
     *
     * @param question the id of the {@code AbstractQuestion}
     * @param topic    the id of the {@code Topic}
     */
    public Refer(int question, int topic) {
        this.question = question;
        this.topic = topic;
    }

    /**
     * Returns the id of the {@code AbstractQuestion}.
     *
     * @return the id of the {@code AbstractQuestion}.
     */
    public final int getQuestion() {
        return question;
    }

    /**
     * Returns the id of the {@code Topic}.
     *
     * @return the id of the {@code Topic}.
     */
    public final int getTopic() {
        return topic;
    }


    /**
     * Creates a JsonGenerator that generates the refer in the JSON format
     *
     * @param out the stream to which the JSON representation of the {@code AbstractResource} has to be written.
     * @throws IOException if something goes wrong while creating the generator.
     */
    @Override
    public final void toJSON(final OutputStream out) throws IOException {

        final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

        jg.writeStartObject();

        jg.writeFieldName("refer");

        jg.writeStartObject();

        jg.writeNumberField("question", question);

        jg.writeNumberField("topic", topic);

        jg.writeEndObject();

        jg.writeEndObject();

        jg.flush();
    }

    /**
     * Creates a {@code Refer} from its JSON representation.
     *
     * @param in the input stream containing the JSON document.
     * @return the {@code Refer} created from the JSON representation.
     * @throws IOException if something goes wrong while parsing.
     */
    public static Refer fromJSON(final InputStream in) throws IOException {

        // the fields read from JSON
        int jQuestion = -1;
        int jTopic = -1;

        final JsonParser jp = JSON_FACTORY.createParser(in);

        // while we are not on the start of an element or the element is not
        // a token element, advance to the next element (if any)
        while (jp.getCurrentToken() != JsonToken.FIELD_NAME || !"refer".equals(jp.getCurrentName())) {

            // there are no more events
            if (jp.nextToken() == null) {
                throw new IOException("Unable to parse JSON: no refer object found.");
            }
        }

        while (jp.nextToken() != JsonToken.END_OBJECT) {

            if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {

                switch (jp.getCurrentName()) {
                    case "question":
                        jp.nextToken();
                        jQuestion = jp.getIntValue();
                        break;
                    case "topic":
                        jp.nextToken();
                        jTopic = jp.getIntValue();
                        break;
                }
            }
        }

        return new Refer(jQuestion, jTopic);
    }
}
