/*
 * Copyright 2018 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.resource;


import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Represents the data about a question.
 *
 * @author Apollo Group
 * @version 1.00
 * @since 1.00
 */
public class QuestionIntInterview extends AbstractQuestion
{
    /**
     * The id of the {@code InterviewIntCompany} to which the question belongs
     */
    private final int interview;

    /**
     * Creates a new question
     *
     * @param id        the id of the question
     * @param category  the id of the {@code Category} to which the question belongs
     * @param title     the title of the question
     * @param text      the text of the question
     * @param interview the id of the {@code InterviewIntCompany} to which the question belongs
     * @param userEmail the email of the {@code User} who asked the question
     */
    public QuestionIntInterview(int id, int category, String title, String text, int interview, String userEmail) {

        super(id, category, title, text, userEmail);
        this.interview = interview;
    }

    /**
     * Returns the {@code InterviewIntCompany} of the question.
     *
     * @return the {@code InterviewIntCompany} of the question.
     */
    public final Integer getInterview() {
        return interview;
    }

    /**
     * Creates a JsonGenerator that generates the question in the JSON format
     *
     * @param out the stream to which the JSON representation of the {@code AbstractResource} has to be written.
     * @throws IOException if something goes wrong while creating the generator.
     */
    public void toJSON(final OutputStream out) throws IOException {

        final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

        jg.writeStartObject();

        jg.writeFieldName("question");

        jg.writeStartObject();

        jg.writeNumberField("id", super.id);

        jg.writeNumberField("category", super.category);

        jg.writeStringField("title", super.title);

        jg.writeStringField("text", super.text);

        jg.writeNumberField("interviewId", this.interview);

        jg.writeStringField("userEmail", super.userEmail);

        jg.writeEndObject();

        jg.writeEndObject();

        jg.flush();
    }

    /**
     * Creates a {@code QuestionIntInterview} from its JSON representation.
     *
     * @param in the input stream containing the JSON document.
     * @return the {@code QuestionIntInterview} created from the JSON representation.
     * @throws IOException if something goes wrong while parsing.
     */
    public static QuestionIntInterview fromJSON(final InputStream in) throws IOException {

        // the fields read from JSON
        int jId = -1;
        int jCategory = -1;
        String jTitle = null;
        String jText = null;
        int jInterviewId = -1;
        String jUserEmail = null;

        final JsonParser jp = JSON_FACTORY.createParser(in);

        // while we are not on the start of an element or the element is not
        // a token element, advance to the next element (if any)
        while (jp.getCurrentToken() != JsonToken.FIELD_NAME || "question".equals(jp.getCurrentName()) == false) {

            // there are no more events
            if (jp.nextToken() == null) {
                throw new IOException("Unable to parse JSON: no question object found.");
            }
        }

        while (jp.nextToken() != JsonToken.END_OBJECT) {

            if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {

                switch (jp.getCurrentName()) {
                    case "id":
                        jp.nextToken();
                        jId = jp.getIntValue();
                        break;
                    case "category":
                        jp.nextToken();
                        jCategory = jp.getIntValue();
                        break;
                    case "title":
                        jp.nextToken();
                        jTitle = jp.getText();
                        break;
                    case "text":
                        jp.nextToken();
                        jText = jp.getText();
                        break;
                    case "interviewId":
                        jp.nextToken();
                        jInterviewId = jp.getIntValue();
                        break;
                    case "userEmail":
                        jp.nextToken();
                        jUserEmail = jp.getText();
                        break;
                }
            }
        }

        return new QuestionIntInterview(jId, jCategory, jTitle, jText, jInterviewId, jUserEmail);
    }
}
