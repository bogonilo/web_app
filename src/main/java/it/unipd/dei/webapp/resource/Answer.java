/*
 * Copyright 2018 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.resource;


import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;

/**
 * Represents the data about an answer.
 *
 * @author Apollo Group
 * @version 1.00
 * @since 1.00
 */
public class Answer extends AbstractResource
{

    /**
     * The id (identifier) of the answer
     */
    private final int id;
    /**
     * The email of the {@code User} that submitted the answer
     */
    private final String userEmail;
    /**
     * The id of the {@code AbstractQuestion} which refers the answer
     */
    private final int question;
    /**
     * The text of the answer
     */
    private final String text;
    /**
     * The date of the answer
     */
    private final Date date;

    /**
     * Creates a new answer
     *
     * @param id        the id of the answer
     * @param userEmail the email of the {@code User} that submitted the answer
     * @param question  the id of the {@code AbstractQuestion} which refers the answer
     * @param text      the text of the answer
     * @param date      the date of the answer
     */
    public Answer(int id, String userEmail, int question, String text, Date date) {
        this.id = id;
        this.userEmail = userEmail;
        this.question = question;
        this.text = text;
        this.date = date;
    }

    /**
     * Returns the id of the answer.
     *
     * @return the id of the answer.
     */
    public final int getId() {
        return id;
    }

    /**
     * Returns the email of the user.
     *
     * @return the email of the user.
     */
    public final String getUser() {
        return userEmail;
    }

    /**
     * Returns the id of the question.
     *
     * @return the id of the question.
     */
    public final int getQuestion() {
        return question;
    }

    /**
     * Returns the text of the answer.
     *
     * @return the text of the answer.
     */
    public final String getText() {
        return text;
    }

    /**
     * Returns the date of the answer.
     *
     * @return the date of the answer.
     */
    public final Date getDate() {
        return date;
    }

    /**
     * Creates a JsonGenerator that generates the answer in the JSON format
     *
     * @param out the stream to which the JSON representation of the {@code AbstractResource} has to be written.
     * @throws IOException if something goes wrong while creating the generator.
     */
    @Override
    public final void toJSON(final OutputStream out) throws IOException {

        final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

        jg.writeStartObject();

        jg.writeFieldName("answer");

        jg.writeStartObject();

        jg.writeNumberField("id", id);

        jg.writeStringField("userEmail", userEmail);

        jg.writeNumberField("question", question);

        jg.writeStringField("text", text);

        jg.writeStringField("date", date.toString());

        jg.writeEndObject();

        jg.writeEndObject();

        jg.flush();
    }

    /**
     * Creates a {@code Answer} from its JSON representation.
     *
     * @param in the input stream containing the JSON document.
     * @return the {@code Answer} created from the JSON representation.
     * @throws IOException if something goes wrong while parsing.
     */
    public static Answer fromJSON(final InputStream in) throws IOException {

        // the fields read from JSON
        int jId = -1;
        String jUser = null;
        int jQuestionId = -1;
        String jText = null;
        Date jDate = null;

        final JsonParser jp = JSON_FACTORY.createParser(in);

        // while we are not on the start of an element or the element is not
        // a token element, advance to the next element (if any)
        while (jp.getCurrentToken() != JsonToken.FIELD_NAME || !"answer".equals(jp.getCurrentName())) {

            // there are no more events
            if (jp.nextToken() == null) {
                throw new IOException("Unable to parse JSON: no answer object found.");
            }
        }

        while (jp.nextToken() != JsonToken.END_OBJECT) {

            if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {

                switch (jp.getCurrentName()) {
                    case "id":
                        jp.nextToken();
                        jId = jp.getIntValue();
                        break;
                    case "userEmail":
                        jp.nextToken();
                        jUser = jp.getText();
                        break;
                    case "question":
                        jp.nextToken();
                        jQuestionId = jp.getIntValue();
                        break;
                    case "text":
                        jp.nextToken();
                        jText = jp.getText();
                        break;
                    case "date":
                        jp.nextToken();
                        jDate = new Date(jp.getText());
                        break;
                }
            }
        }

        return new Answer(jId, jUser, jQuestionId, jText, jDate);
    }
}
