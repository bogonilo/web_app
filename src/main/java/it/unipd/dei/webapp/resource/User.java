/*
 * Copyright 2018 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.resource;


import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Date;

/**
 * Represents the data about an user.
 *
 * @author Apollo Group
 * @version 1.00
 * @since 1.00
 */
public class User extends AbstractResource
{

    public enum ROLE {USER, ADMIN}

    /**
     * The email (identifier) of the user
     */
    private final String email;

    /**
     * The username of the user
     */
    private final String username;

    /**
     * The password of the user
     */
    private final String password;

    /**
     * The age of the user
     */
    private final Date birthday;

    /**
     * The role of the user
     */
    private final ROLE role;

    /**
     * Creates a new user
     *
     * @param email    the email of the user
     * @param username the username of the user
     * @param password the password of the user
     * @param birthday      the age of the user
     * @param role     the role of the user
     */
    public User(String email, String username, String password, Date birthday, ROLE role) {
        this.email = email;
        this.username = username;
        this.password = password;
        this.birthday = birthday;
        this.role = role;
    }

    /**
     * Returns the email of the user
     *
     * @return the email of the user
     */
    public final String getEmail() {
        return email;
    }

    /**
     * Returns the username of the user
     *
     * @return the username of the user
     */
    public final String getUsername() {
        return username;
    }

    /**
     * Returns the password of the user
     *
     * @return the password of the user
     */
    public final String getPassword() {
        return password;
    }

    /**
     * Returns the age of the user
     *
     * @return the age of the user
     */
    public final Date getBirthday() {
        return birthday;
    }

    /**
     * Returns the role of the user
     *
     * @return the role of the user
     */
    public final ROLE getRole() {
        return role;
    }

    /**
     * Creates a JsonGenerator that generates the user in the JSON format
     *
     * @param out the stream to which the JSON representation of the {@code AbstractResource} has to be written.
     * @throws IOException if something goes wrong while creating the generator.
     */
    @Override
    public final void toJSON(final OutputStream out) throws IOException {

        final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

        jg.writeStartObject();

        jg.writeFieldName("user");

        jg.writeStartObject();

        jg.writeStringField("email", email);

        jg.writeStringField("username", username);

        jg.writeStringField("password", password);

        jg.writeStringField("birthday", birthday.toString());

        jg.writeStringField("role", role.toString());

        jg.writeEndObject();

        jg.writeEndObject();

        jg.flush();
    }

    /**
     * Creates a {@code User} form its JSON representation
     *
     * @param in the input stream containing the JSON document.
     * @return the {@code User} created from the JSON representation.
     * @throws IOException if something goes wrong while parsing.
     */
    public static User fromJSON(final InputStream in) throws IOException {

        // the fields read from JSON
        String jEmail = null;
        String jUsername = null;
        String jPassword = null;
        Date jBirthday = null;
        String jRole = null;
        ROLE role = null;

        final JsonParser jp = JSON_FACTORY.createParser(in);

        // while we are not on the start of an element or the element is not
        // a token element, advance to the next element (if any)
        while (jp.getCurrentToken() != JsonToken.FIELD_NAME || !"user".equals(jp.getCurrentName())) {

            // there are no more events
            if (jp.nextToken() == null) {
                throw new IOException("Unable to parse JSON: no user object found.");
            }
        }

        while (jp.nextToken() != JsonToken.END_OBJECT) {

            if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {

                switch (jp.getCurrentName()) {
                    case "email":
                        jp.nextToken();
                        jEmail = jp.getText();
                        break;
                    case "username":
                        jp.nextToken();
                        jUsername = jp.getText();
                        break;
                    case "password":
                        jp.nextToken();
                        jPassword = jp.getText();
                        break;
                    case "birthday":
                        jp.nextToken();
                        jBirthday = Date.valueOf(jp.getText());
                        break;
                    case "role":
                        jp.nextToken();
                        jRole = jp.getText();
                        break;
                }
            }
        }

        assert jRole != null;
        switch (jRole) {
            case "admin":
                role = ROLE.ADMIN;
                break;
            case "ADMIN":
                role = ROLE.ADMIN;
                break;
            case "user":
                role = ROLE.USER;
                break;
            case "USER":
                role = ROLE.USER;
                break;
        }

        return new User(jEmail, jUsername, jPassword, jBirthday, role);
    }
}
