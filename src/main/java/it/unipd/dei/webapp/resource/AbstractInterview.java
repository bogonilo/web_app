/*
 * Copyright 2018 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.resource;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Date;

/**
 * Represents the data about an interview.
 *
 * @author Apollo Group
 * @version 1.00
 * @since 1.00
 */
public abstract class AbstractInterview extends AbstractResource
{
    /**
     * The id (identifier) of the interview
     */
    protected final int id;
    /**
     * The date of the interview
     */
    protected final Date date;
    /**
     * The title of the interview
     */
    protected final String title;

    /**
     * Creates a new interview
     *
     * @param id      the id of the interview
     * @param date    the date of the interview
     * @param title the title of the interview
     */
    public AbstractInterview(int id, Date date, String title) {
        this.id = id;
        this.date = date;
        this.title = title;
    }

    /**
     * Returns the id of the interview.
     *
     * @return the id of the interview.
     */
    public final int getId() { return id; }

    /**
     * Returns the date of the interview.
     *
     * @return the date of the interview.
     */
    public final Date getDate() {
        return date;
    }

    /**
     * Returns the title of the interview.
     *
     * @return the title of the interview.
     */
    public final String getTitle() {
        return title;
    }

    /**
     * Returns the company of the interview.
     *
     * @return the company of the interview.
     */
    public abstract Object getCompany();

    /**
     * Creates a JsonGenerator that generates the interview in the JSON format
     *
     * @param out the stream to which the JSON representation of the {@code AbstractResource} has to be written.
     * @throws IOException if something goes wrong while creating the generator.
     */
    @Override
    public abstract void toJSON(final OutputStream out) throws IOException;
}
