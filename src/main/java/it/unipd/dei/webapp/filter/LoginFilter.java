package it.unipd.dei.webapp.filter;

import it.unipd.dei.webapp.resource.User;

import javax.servlet.*;
import java.io.IOException;
import javax.servlet.http.*;


/**
 * The doFilter method of the Filter is called by the container each time a request/response pair
 * is passed through the chain due to a client request for a resource at the end of the chain.
 * The FilterChain passed in to this method allows the Filter to pass on the request and response to the next entity in the chain.
 *
 * @author Apollo group
 * @version 1.00
 * @since 1.00
 **/
public class LoginFilter implements Filter {
    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws ServletException, IOException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        String path = request.getRequestURI();
        HttpSession session = request.getSession();
        String loginURI = request.getContextPath() + "/login";
        User user = (User) session.getAttribute("user");

        boolean isCommonPage = false;
        boolean isLoggedPage = false;
        boolean isDesign = false;
        boolean isAdmin = false;

        boolean js = false;
        boolean css = false;
        boolean img = false;

        if ( //common page
                path.equals("/w2018_apollo/login") ||
                        path.equals("/w2018_apollo/logout") ||
                        path.equals("/w2018_apollo/") ||
                        path.equals("/w2018_apollo/create-user") ||
                        path.equals("/w2018_apollo/rest/question/") ||
                        path.contains("/w2018_apollo/question/") ||
                        path.contains("/w2018_apollo/rest/category/") ||
                        path.contains("/w2018_apollo/rest/interview/") ||
                        path.contains("/w2018_apollo/rest/question/")
                ) {

            isCommonPage = true;


        } else if ( //design page

                path.contains("/w2018_apollo/js/") ||
                        path.contains("/w2018_apollo/css/") ||
                        path.contains("/w2018_apollo/img/")

                ) {

            isDesign = true;

        } else { //permission required

            if (session != null && user != null) { //logged

                if (user.getRole() == User.ROLE.ADMIN) { //Is admin

                    isAdmin = true;

                } else {

                    //Check the legitimate of the page request
                    if (
                            path.contains("/w2018_apollo/rest/question/") ||
                            path.contains("/w2018_apollo/rest/interview/") ||
                            path.contains("/w2018_apollo/rest/answer/") ||
                            path.contains("/w2018_apollo/rest/category/") ||
                            path.contains("/w2018_apollo/question/") ||
                            path.contains("/w2018_apollo/interview/") ||
                            path.contains("/w2018_apollo/create-question") ||
                            path.contains("/w2018_apollo/create-answer") ||
                            path.contains("/w2018_apollo/create-interview") ||
                            path.contains("/w2018_apollo/jsp/")

                            ) {
                        isLoggedPage = true;
                    }


                }
            }
        }


        if ((isLoggedPage || isCommonPage || isDesign || isAdmin) && !path.contains("..")) {
            chain.doFilter(request, response);
        } else {
            response.sendRedirect(loginURI);
        }
    }

}

