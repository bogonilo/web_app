package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.database.CreateInterviewDatabase;
import it.unipd.dei.webapp.resource.InterviewIntCompany;
import it.unipd.dei.webapp.resource.Message;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;


/**
 * Create new interview into database.
 *
 * @author Apollo group
 * @version 1.00
 * @since 1.00
 */
public final class CreateInterviewServlet extends AbstractDatabaseServlet{

    /**
     * Creates a new interview session into database.
     *
     * @param req
     *            the HTTP request from the client.
     * @param res
     *            the HTTP response from the server.
     *
     * @throws ServletException
     *             if any error occurs while executing the servlet.
     * @throws IOException if any error occurs in the client/server communication.
     */
    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {

        // request parameters
        int id = -1; //default
        int company = 1;
        Date date = null;
        String title = "";

        // model
        InterviewIntCompany i  = null;
        Message m = null;

        try{
            // retrieves the request parameters
            company = Integer.parseInt(req.getParameter("company"));
            date = new Date(req.getDateHeader("date"));
            title = req.getParameter("title");

            //creates a new interview from the request parameters
            i = new InterviewIntCompany(id, company, date, title);

            // creates a new object for accessing the database and stores the interview
            new CreateInterviewDatabase(getDataSource().getConnection(), i).createInterview();

            m = new Message(String.format("Interview %s successfully created.", title));

        } catch (NumberFormatException ex) {
            m = new Message("Cannot create the interview. Invalid input parameters.",
                    "E100", ex.getMessage());
        } catch (SQLException ex) {
            if (ex.getSQLState().equals("23505")) {
                m = new Message(String.format("Cannot create the interview: interview %s already exists.", id),
                        "E300", ex.getMessage());
            } else {
                m = new Message("Cannot create the interview: unexpected error while accessing the database.",
                        "E200", ex.getMessage());
            }
        }

        // stores the interview and the message as a request attribute
        req.setAttribute("interview", i);
        req.setAttribute("message", m);

        // forwards the control to the create-user-result JSP
        req.getRequestDispatcher("/jsp/create-interview-result.jsp").forward(req, res);
    }

}
