/*
 * Copyright 2018 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.database.CreateQuestionDatabase;
import it.unipd.dei.webapp.resource.AbstractQuestion;
import it.unipd.dei.webapp.resource.Message;
import it.unipd.dei.webapp.resource.QuestionIntInterview;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Creates a new question into the database.
 * 
 * @author Apollo grupo
 * @version 1.00
 * @since 1.00
 */
public final class CreateQuestionServlet extends AbstractDatabaseServlet {

	/**
	 * Creates a new question into the database.
	 * 
	 * @param req
	 *            the HTTP request from the client.
	 * @param res
	 *            the HTTP response from the server.
	 *
	 * @throws ServletException
	 *             if any error occurs while executing the servlet.
	 * @throws IOException if any error occurs in the client/server communication.
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException
	{

		// request parameters
		int id = -1;
		int category = -1;
		String title = null;
		String text = null;
		int interviewId = -1;
		String userId = null;

		// model
		QuestionIntInterview q = null;
		Message m = null;

		try
		{
			// retrieves the request parameters
			category = Integer.parseInt(req.getParameter("category"));
			title = req.getParameter("title");
			text = req.getParameter("text");
			interviewId = Integer.parseInt(req.getParameter("interview"));
			userId = req.getParameter("userEmail");

			// creates a new question from the request parameters
			q = new QuestionIntInterview(id, category, title, text, interviewId, userId);

			// creates a new object for accessing the database and stores the question
			new CreateQuestionDatabase(getDataSource().getConnection(), q).createQuestion();

			m = new Message("AbstractQuestion successfully created.");

		} catch (NumberFormatException ex)
		{
			m = new Message(String.format("Cannot create the question. Invalid input parameters. <br/> %d <br/> <br/> %d <br/> %s <br/> %s <br/> %d <br/> %s <br/>", category, title, text, interviewId, userId),
					"E100", ex.getMessage());
		} catch (SQLException ex)
		{
			if (ex.getSQLState().equals("23505"))
			{
				m = new Message("Cannot create the question: question already exists.",
						"E300", ex.getMessage());
			} else
			{
				m = new Message("Cannot create the question: unexpected error while accessing the database. ",
						"E200", ex.getMessage());
			}
		}

		if (q == null)
		{
			// stores the question and the message as a request attribute
			req.setAttribute("question", q);
			req.setAttribute("message", m);

			// forwards the control to the create-question-result JSP
			//req.getRequestDispatcher("/jsp /create-question-result.jsp").forward(req, res);
			req.getRequestDispatcher("/jsp/create-question-result.jsp").forward(req, res);
		}
		else
			res.sendRedirect("http://localhost:8081/w2018_apollo/interview/"+interviewId);
	}

}
