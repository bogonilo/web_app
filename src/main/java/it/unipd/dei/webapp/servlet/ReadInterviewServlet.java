package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.database.ReadInterviewDatabase;
import it.unipd.dei.webapp.resource.Message;
import it.unipd.dei.webapp.resource.InterviewIntCompany;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;


/**
 * Servlet who manages a interview. The interview is read from the database and showed in interview-info.jsp
 *
 * @author Apollo group
 * @version 1.00
 * @since 1.00
 */
public class ReadInterviewServlet extends AbstractDatabaseServlet
{
    /**
     * Manages HTTP GET requests for interview
     *
     * @param req
     *            the request from the client.
     * @param res
     *            the response from the server.
     *
     * @throws ServletException
     *             if any problem occurs while executing the servlet.
     * @throws IOException
     *             if any problem occurs while communicating between the client
     *             and the server.
     */
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {

        String path = req.getRequestURI();
        int questionId = Integer.parseInt(path.substring(path.lastIndexOf("/interview") + 11));

        InterviewIntCompany i = null;
        Message m = null;

        try
        {
            i= new ReadInterviewDatabase(getDataSource().getConnection(), questionId).readInterview();

        } catch (SQLException e)
        {
            if (e.getSQLState().equals("23505")) {
                m = new Message("Cannot create the question: question already exists.",
                        "E300", e.getMessage());
            } else {
                m = new Message("Cannot create the question: unexpected error while accessing the database. ",
                        "E200", e.getMessage());
            }
            e.printStackTrace();
        }

        req.setAttribute("interview", i);
        req.setAttribute("message", m);

        req.getRequestDispatcher("/jsp/interview-info.jsp").forward(req, res);

    }
}
