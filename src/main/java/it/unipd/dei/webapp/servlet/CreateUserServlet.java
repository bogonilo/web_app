/*
 * Copyright 2018 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.database.CreateUserDatabase;
import it.unipd.dei.webapp.resource.User;
import it.unipd.dei.webapp.resource.Message;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * Creates a new user into the database.
 * 
 * @author Apollo group
 * @version 1.00
 * @since 1.00
 */
public final class CreateUserServlet extends AbstractDatabaseServlet {

	/**
	 * Manages HTTP GET requests for a new user"
	 *
	 * @param req
	 *            the request from the client.
	 * @param res
	 *            the response from the server.
	 *
	 * @throws ServletException
	 *             if any problem occurs while executing the servlet.
	 * @throws IOException if any problem occurs while communicating between the client and the server.
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
	{
		req.getRequestDispatcher("/jsp/create-user-form.jsp").forward(req, res);

	}

	/**
	 * Creates a new user into the database.
	 * 
	 * @param req
	 *            the HTTP request from the client.
	 * @param res
	 *            the HTTP response from the server.
	 *
	 * @throws ServletException
	 *             if any error occurs while executing the servlet.
	 * @throws IOException if any error occurs in the client/server communication.
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		// request parameters
		String email = null;
		String username = null;
		String passwordMD5HEX = null;
		Date birthday = null;
		User.ROLE role = User.ROLE.USER;

		// model
		User e  = null;
		Message m = null;

		try{
			// retrieves the request parameters
			email = req.getParameter("email");
			username = req.getParameter("username");
			passwordMD5HEX = DigestUtils.md5Hex(req.getParameter("password")).toUpperCase();
			birthday = Date.valueOf(req.getParameter("bday"));

			// creates a new user from the request parameters
			e = new User(email, username, passwordMD5HEX, birthday, role);

			// creates a new object for accessing the database and stores the user
			new CreateUserDatabase(getDataSource().getConnection(), e).createUser();
			
			m = new Message(String.format("User %s successfully created.", username));

		} catch (NumberFormatException ex) {
			m = new Message(String.format("User already exist or wrong parameter!", username),
					"", "");
		} catch (SQLException ex) {
			if (ex.getSQLState().equals("23505")) {
				m = new Message(String.format("User already exist or wrong parameter!", username),
						"", "");
			} else {
				m = new Message(String.format("User already exist or wrong parameter!", username),
						"", "");
			}
		}
		
		// stores the user and the message as a request attribute
		req.setAttribute("user", e);
		req.setAttribute("message", m);

		req.getRequestDispatcher("/jsp/login-form.jsp").forward(req, res);
	}

}
