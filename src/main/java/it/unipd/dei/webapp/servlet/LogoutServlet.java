package it.unipd.dei.webapp.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Servlet to execute the logout procedure
 *
 * @author Apollo group
 * @version 1.00
 * @since 1.00
 */
public class LogoutServlet extends AbstractDatabaseServlet {

    /**
     * Manages HTTP GET requests in the logout procedure
     *
     * @param req
     *            the request from the client.
     * @param res
     *            the response from the server.
     *
     * @throws ServletException
     *             if any problem occurs while executing the servlet.
     * @throws IOException
     *             if any problem occurs while communicating between the client
     *             and the server.
     */
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        HttpSession session = req.getSession();

        if (session.getAttribute("user") != null) {
            session.removeAttribute("user");
            session.invalidate();

            Cookie username = new Cookie("username", "");
            username.setPath("/");
            username.setMaxAge(0); //delete cookie
            res.addCookie(username);
            Cookie useremail = new Cookie("useremail", "");
            useremail.setPath("/");
            useremail.setMaxAge(0); //delete cookie
            res.addCookie(useremail);

            req.getRequestDispatcher("/jsp/index.jsp").forward(req, res);
        } else {


            Cookie username = new Cookie("username", "");
            username.setPath("/");
            username.setMaxAge(0); //delete cookie
            res.addCookie(username);
            Cookie useremail = new Cookie("useremail", "");
            useremail.setPath("/");
            useremail.setMaxAge(0); //delete cookie
            res.addCookie(useremail);

            req.getRequestDispatcher("/jsp/index.jsp").forward(req, res);

        }
    }

}
