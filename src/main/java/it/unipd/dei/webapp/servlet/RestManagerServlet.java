/*
 * Copyright 2018 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.resource.*;
import it.unipd.dei.webapp.rest.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Manages the REST API for the different REST resources.
 *
 * @author Apollo group
 * @version 1.00
 * @since 1.00
 */
public final class RestManagerServlet extends AbstractDatabaseServlet {


    /**
     * The JSON MIME media type
     */
    private static final String JSON_MEDIA_TYPE = "application/json";

	/**
	 * The JSON UTF-8 MIME media type
	 */
	private static final String JSON_UTF_8_MEDIA_TYPE = "application/json; charset=utf-8";

	/**
	 * The any MIME media type
	 */
	private static final String ALL_MEDIA_TYPE = "*/*";

	@Override
	protected final void service(final HttpServletRequest req, final HttpServletResponse res)
			throws IOException {

		res.setContentType(JSON_UTF_8_MEDIA_TYPE);
		final OutputStream out = res.getOutputStream();

		try {
			// if the request method and/or the MIME media type are not allowed, return.
			// Appropriate error message sent by {@code checkMethodMediaType}
			if (!checkMethodMediaType(req, res)) {
				return;
			}

			// if the requested resource was an User, delegate its processing and return
			if (processUser(req, res)) {
				return;
			}

            //if the requested resource was a Question, delegate its processing and return
            if(processQuestion(req, res)){
                return;
            }

            //if the requested resource was a Interview, delegate its processing and return
            if(processInterview(req, res)){
                return;
            }

            //if the requested resource was a Category, delegate its processing and return
            if(processCategory(req, res)){
                return;
            }

            //if the requested resource was an Answer, delegate its processing and return
            if(processAnswer(req, res)){
                return;
            }

            // if none of the above process methods succeeds, it means an unknown resource has been requested
            final Message m = new Message("Unknown resource requested.", "E4A6",
                    String.format("Requested resource is %s.", req.getRequestURI()));
            res.setStatus(HttpServletResponse.SC_NOT_FOUND);
            m.toJSON(out);
        } finally {
            // ensure to always flush and close the output stream
            out.flush();
            out.close();
        }
    }

	/**
	 * Checks that the request method and MIME media type are allowed.
	 *
	 * @param req the HTTP request.
	 * @param res the HTTP response.
	 * @return {@code true} if the request method and the MIME type are allowed; {@code false} otherwise.
	 *
	 * @throws IOException if any error occurs in the client/server communication.
	 */
	private boolean checkMethodMediaType(final HttpServletRequest req, final HttpServletResponse res)
			throws IOException {

		final String method = req.getMethod();
		final String contentType = req.getHeader("Content-Type");
		final String accept = req.getHeader("Accept");
		final OutputStream out = res.getOutputStream();

		Message m = null;

        if(accept == null) {
            m = new Message("Output media type not specified.", "E4A1", "Accept request header missing.");
            res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            m.toJSON(out);
            return false;
        }

//		if(!accept.contains(JSON_MEDIA_TYPE) && !accept.equals(ALL_MEDIA_TYPE)) {
//			m = new Message("Unsupported output media type. Resources are represented only in application/json.",
//							"E4A2", String.format("Requested representation is %s.", accept));
//			res.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
//			m.toJSON(out);
//			return false;
//		}

        switch(method) {
            case "GET":
            case "DELETE":
                // nothing to do
                break;

			case "POST":
			case "PUT":
				if(contentType == null) {
					m = new Message("Input media type not specified.", "E4A3", "Content-Type request header missing.");
					res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
					m.toJSON(out);
					return false;
				}

				if(!contentType.contains(JSON_MEDIA_TYPE)) {
					m = new Message("Unsupported input media type. Resources are represented only in application/json.",
									"E4A4", String.format("Submitted representation is %s.", contentType));
					res.setStatus(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE);
					m.toJSON(out);
					return false;
				}

				break;
			default:
				m = new Message("Unsupported operation.",
								"E4A5", String.format("Requested operation %s.", method));
				res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
				m.toJSON(out);
				return false;
		}

		return true;
	}


	/**
	 * Checks whether the request if for an {@link User} resource and, in case, processes it.
	 *
	 * @param req the HTTP request.
	 * @param res the HTTP response.
	 * @return {@code true} if the request was for an {@code User}; {@code false} otherwise.
	 *
	 * @throws IOException if any error occurs in the client/server communication.
	 */
	private boolean processUser(HttpServletRequest req, HttpServletResponse res) throws IOException {

		final String method = req.getMethod();

		String path = req.getRequestURI();
		Message m = null;

		// the requested resource was not an user
		if(path.lastIndexOf("rest/user") <= 0) {
			return false;
		}

		try {
			// strip everything until after the /user
			path = path.substring(path.lastIndexOf("user") + 4);

			// the request URI is: /user
			// if method GET, list users
			// if method POST, create user
			if (path.length() == 0 || path.equals("/")) {

				switch (method) {
					case "POST":
						new UserRestResource(req, res, getDataSource().getConnection()).createUser();
						break;
					default:
						m = new Message("Unsupported operation for URI /user.",
										"E4A5", String.format("Requested operation %s.", method));
						res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
						m.toJSON(res.getOutputStream());
						break;
				}
			} else {
                    // the request URI is: /user/{email}
                    try {

                        switch (method) {
                            case "GET":
                                new UserRestResource(req, res, getDataSource().getConnection()).readUser();
                                break;
                            case "PUT":
                                new UserRestResource(req, res, getDataSource().getConnection()).updateUser();
                                break;
                            case "DELETE":
                                new UserRestResource(req, res, getDataSource().getConnection()).deleteUser();
                                break;
                            default:
                                m = new Message("Unsupported operation for URI /user/{email}.",
                                        "E4A5", String.format("Requested operation %s.", method));
                                res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                                m.toJSON(res.getOutputStream());
                        }
                    } catch (NumberFormatException e) {
                        m = new Message("Wrong parameter for URI /user/{email}.",
                                "E4A7", e.getMessage());
                        res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        m.toJSON(res.getOutputStream());
                    }
            }
        } catch(Throwable t) {
            m = new Message("Unexpected error.", "E5A1", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }

        return true;

    }

    /**
     * Checks whether the request if for an {@link AbstractQuestion} resource and, in case, processes it.
     *
     * @param req the HTTP request.
     * @param res the HTTP response.
     * @return {@code true} if the request was for an {@code AbstractQuestion}; {@code false} otherwise.
     *
     * @throws IOException if any error occurs in the client/server communication.
     */
    private boolean processQuestion(HttpServletRequest req, HttpServletResponse res) throws IOException {

        final String method = req.getMethod();

        String path = req.getRequestURI();
        Message m = null;

        // the requested resource was not a question
        if(path.lastIndexOf("rest/question") <= 0) {
            return false;
        }

        try {
            // strip everything until after the /question
            path = path.substring(path.lastIndexOf("question") + 8);

            // the request URI is: /question
            // if method GET, list question
            // if method POST, create question
            if (path.length() == 0 || path.equals("/")) {

                switch (method) {
                    case "GET":
                        new QuestionRestResource(req, res, getDataSource().getConnection()).listQuestion();
                        break;
                    case "POST":
                        new QuestionRestResource(req, res, getDataSource().getConnection()).createQuestion();
                        break;
                    default:
                        m = new Message("Unsupported operation for URI /question.",
                                "E4A5", String.format("Requested operation %s.", method));
                        res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                        m.toJSON(res.getOutputStream());
                        break;
                }

            } else if (path.contains("category")) {

                    // the request URI is: /question/category/{categoryId}

                    try {

                        switch (method) {
                            case "GET":

                                // check that the parameter is actually an integer
                                int categoryId = Integer.parseInt(path.substring(path.lastIndexOf("/category") + 10));
                                if (categoryId < 1 || categoryId > 100){
                                    throw new NumberFormatException();
                                }

                                new QuestionRestResource(req, res, getDataSource().getConnection()).listCategoryQuestion(categoryId);
                                break;

                            default:
                                m = new Message("Unsupported operation for URI /question/category/{categoryId}.",
                                        "E4A4", String.format("Requested operation %s.", method));
                                res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                                m.toJSON(res.getOutputStream());
                        }
                    } catch (NumberFormatException e) {
                        m = new Message("Wrong format for URI /question/category/{categoryId}: {categoryId} is not an integer.",
                                "E4A7", e.getMessage());
                        res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        m.toJSON(res.getOutputStream());
                    }
            } else {
                    // the request URI is: /question/{questionId}
                    try {
                        switch (method) {
                            case "GET":
                                new QuestionRestResource(req, res, getDataSource().getConnection()).readQuestion();
                                break;

                            case "DELETE":
                                new QuestionRestResource(req, res, getDataSource().getConnection()).deleteQuestion();
                                break;
                            default:
                                m = new Message("Unsupported operation for URI /question/{questionId}.",
                                        "E4A5", String.format("Requested operation %s.", method));
                                res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                                m.toJSON(res.getOutputStream());
                        }
                    } catch (NumberFormatException e) {
                        m = new Message("Wrong format for URI /question/{questionId}: {questionId} is not an integer.",
                                "E4A7", e.getMessage());
                        res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        m.toJSON(res.getOutputStream());
                    }
            }
        } catch(Throwable t) {
            m = new Message("Unexpected error.", "E5A1", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }

        return true;

    }

    /**
     * Checks whether the request if for an {@link InterviewIntCompany} resource and, in case, processes it.
     *
     * @param req the HTTP request.
     * @param res the HTTP response.
     * @return {@code true} if the request was for an {@code {@link InterviewIntCompany }}; {@code false} otherwise.
     *
     * @throws IOException if any error occurs in the client/server communication.
     */
    private boolean processInterview(HttpServletRequest req, HttpServletResponse res) throws IOException {

        final String method = req.getMethod();

        String path = req.getRequestURI();
        Message m = null;

        // the requested resource was not a interview
        if(path.lastIndexOf("rest/interview") <= 0) {
            return false;
        }

        try {
            // strip everything until after the /interview
            path = path.substring(path.lastIndexOf("interview") + 9);

            // the request URI is: /interview
            // if method GET, list interview
            if (path.length() == 0 || path.equals("/")) {

                switch (method) {
                    case "GET":
                        new InterviewRestResource(req, res, getDataSource().getConnection()).listInterview();
                        break;
                    default:
                        m = new Message("Unsupported operation for URI /interview.",
                                "E4A5", String.format("Requested operation %s.", method));
                        res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                        m.toJSON(res.getOutputStream());
                        break;
                }

            } else {
                // the request URI is: /interview/{interviewId}
                try {
                    // check that the parameter is actually an integer
                    int interviewId = Integer.parseInt(path.substring(1));
                    if (interviewId < 1){
                        throw new NumberFormatException();
                    }

                    switch (method) {
                        case "GET":
                            new InterviewRestResource(req, res, getDataSource().getConnection()).ListQuestionByInterview(interviewId);
                            break;
                        default:
                            m = new Message("Unsupported operation for URI /interview/{interviewId}.",
                                    "E4A5", String.format("Requested operation %s.", method));
                            res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                            m.toJSON(res.getOutputStream());
                    }
                } catch (NumberFormatException e) {
                    m = new Message("Wrong format for URI /interview/{interviewId}: {interviewId} is not an integer.",
                            "E4A7", e.getMessage());
                    res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    m.toJSON(res.getOutputStream());
                }
            }
        } catch(Throwable t) {
            m = new Message("Unexpected error.", "E5A1", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }

        return true;

    }

    /**
     * Checks whether the request if for an {@link Category} resource and, in case, processes it.
     *
     * @param req the HTTP request.
     * @param res the HTTP response.
     * @return {@code true} if the request was for an {@code {@link Category}}; {@code false} otherwise.
     *
     * @throws IOException if any error occurs in the client/server communication.
     */
    private boolean processCategory(HttpServletRequest req, HttpServletResponse res) throws IOException {

        final String method = req.getMethod();

        String path = req.getRequestURI();
        Message m = null;

        // the requested resource was not a category
        if(path.lastIndexOf("rest/category") <= 0) {
            return false;
        }

        try {
            // strip everything until after the /category
            path = path.substring(path.lastIndexOf("category") + 8);

            // the request URI is: /category
            // if method GET, list category
            if (path.length() == 0 || path.equals("/")) {

                switch (method) {
                    case "GET":
                        new CategoryRestResource(req, res, getDataSource().getConnection()).listCategory();
                        break;
                    default:
                        m = new Message("Unsupported operation for URI /category.",
                                "E4A5", String.format("Requested operation %s.", method));
                        res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                        m.toJSON(res.getOutputStream());
                        break;
                }
            }
            else {
                m = new Message("Cannot search list: unexpected error.", "E5A1", "URL error");
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch(Throwable t) {
            m = new Message("Unexpected error.", "E5A1", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }

        return true;

    }

    /**
     * Checks whether the request if for an {@link Answer} resource and, in case, processes it.
     *
     * @param req the HTTP request.
     * @param res the HTTP response.
     * @return {@code true} if the request was for an {@code Answer}; {@code false} otherwise.
     *
     * @throws IOException if any error occurs in the client/server communication.
     */
    private boolean processAnswer(HttpServletRequest req, HttpServletResponse res) throws IOException {

        final String method = req.getMethod();

        String path = req.getRequestURI();
        Message m = null;

        // the requested resource was not an answer
        if(path.lastIndexOf("rest/answer") <= 0) {
            return false;
        }

        try {
            // strip everything until after the /answer
            path = path.substring(path.lastIndexOf("answer") + 6);

            // the request URI is: /answer
            // if method GET
            // if method POST, create answer
            if (path.length() == 0 || path.equals("/")) {

                switch (method) {
                    case "GET": //todo: va implementato?
                     // Non credo esista il caso in cui vogliamo leggere tutte le risposte del database
                     //   new AnswerRestResource(req, res, getDataSource().getConnection()).listAnswer();
                        break;
                    case "POST":
                        new AnswerRestResource(req, res, getDataSource().getConnection()).createAnswer();
                        break;
                    default:
                        m = new Message("Unsupported operation for URI /answer.",
                                "E4A5", String.format("Requested operation %s.", method));
                        res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                        m.toJSON(res.getOutputStream());
                        break;
                }

            } else if (path.contains("question")) {  // todo: è brutto da vedere, ma è giusto, se mai penseremo ad un altra implementazione

                // the request URI is: /answer/question/{questionId}

                try {

                    switch (method) {
                        case "GET":

                            // check that the parameter is actually an integer
                            int questionId = Integer.parseInt(path.substring(path.lastIndexOf("/question") + 10));
                            if (questionId < 1)
                            {
                                throw new NumberFormatException();
                            }

                            new AnswerRestResource(req, res, getDataSource().getConnection()).listAnswerByQuestion(questionId);
                            break;

                        default:
                            m = new Message("Unsupported operation for URI /answer/question/{questionId}.",
                                    "E4A4", String.format("Requested operation %s.", method));
                            res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                            m.toJSON(res.getOutputStream());
                    }
                } catch (NumberFormatException e) {
                    m = new Message("Wrong format for URI /answer/question/{questionId}: {questionId} is not an integer.",
                            "E4A7", e.getMessage());
                    res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    m.toJSON(res.getOutputStream());
                }
            } else if (path.contains("user")) {

                // the request URI is: /answer/user/{userEmail}

                try {

                    switch (method) {
                        case "GET":
                            // check that the parameter is a string
                            String userEmail = path.substring(path.lastIndexOf("/user") + 6);

                            if (userEmail.equalsIgnoreCase(""))
                            {
                                throw new NullPointerException();
                            }

                            new AnswerRestResource(req, res, getDataSource().getConnection()).listAnswerByUser(userEmail);
                            break;

                        default:
                            m = new Message("Unsupported operation for URI /answer/question/{questionId}.",
                                    "E4A4", String.format("Requested operation %s.", method));
                            res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                            m.toJSON(res.getOutputStream());
                    }
                } catch (NullPointerException e) { //todo: controllare se questo errore ha senso
                    m = new Message("Wrong format for URI /answer/user/{userEmail}: {userEmail} should be specified.",
                            "E4A7", e.getMessage());
                    res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    m.toJSON(res.getOutputStream());
                }
            }
            else {
                // the request URI is: /answer/{answerID}
                try {
                    // check that the parameter is actually an integer
                    int questionId = Integer.parseInt(path.substring(1));

                    switch (method) {
                        case "GET":
                            //todo: nella nostra applicazione esiste il caso in cui leggiamo una singola risposta?
                            new AnswerRestResource(req, res, getDataSource().getConnection()).readAnswer();
                            break;

                        case "DELETE":
                            new AnswerRestResource(req, res, getDataSource().getConnection()).deleteAnswer();
                            break;
                        default:
                            m = new Message("Unsupported operation for URI /answer/{answerId}.",
                                    "E4A5", String.format("Requested operation %s.", method));
                            res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                            m.toJSON(res.getOutputStream());
                    }
                } catch (NumberFormatException e) {
                    m = new Message("Wrong format for URI /answer/{answerId}: {answerId} is not an integer.",
                            "E4A7", e.getMessage());
                    res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    m.toJSON(res.getOutputStream());
                }
            }
        } catch(Throwable t) {
            m = new Message("Unexpected error.", "E5A1", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }

        return true;

    }
}