package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.database.CreateAnswerDatabase;
import it.unipd.dei.webapp.resource.Message;
import it.unipd.dei.webapp.resource.Answer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;

/**
 * Creates new answer into the database.
 *
 * @author Apollo group
 * @version 1.00
 * @since 1.00
 */
public class CreateAnswerServlet extends AbstractDatabaseServlet
{
    /**
     * Creates a new answer into the database.
     *
     * @param req
     *            the HTTP request from the client.
     * @param res
     *            the HTTP response from the server.
     *
     * @throws ServletException
     *             if any error occurs while executing the servlet.
     * @throws IOException
     *             if any error occurs in the client/server communication.
     */
    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {

        // request parameters
        int id = -1;
        String userId = null;
        int question = -1;
        String text = null;
        String s_date = null;
        Date date = null;


        // model
        Answer a  = null;
        Message m = null;

        try{
            // retrieves the request parameters
            userId = req.getParameter("userEmail");
            question = Integer.parseInt(req.getParameter("question"));
            text = req.getParameter("text");
            s_date = req.getParameter("date");
            date = Date.valueOf(s_date);


            // creates a new question from the request parameters
            a = new Answer(id, userId, question, text, date);

            // creates a new object for accessing the database and stores the question
            new CreateAnswerDatabase(getDataSource().getConnection(), a).createAnswer();

            m = new Message("Answer successfully created.");

        } catch (NumberFormatException ex) {
            m = new Message(String.format("Cannot create the answer. Invalid input parameters. <br/> %s <br/> <br/> %d <br/> %s <br/> %s <br/>", userId, question, text, date),
                    "E100", ex.getMessage());
        } catch (SQLException ex) {
            if (ex.getSQLState().equals("23505")) {
                m = new Message("Cannot create the answer: answer already exists.",
                        "E300", ex.getMessage());
            } else {
                m = new Message("Cannot create the answer: unexpected error while accessing the database. ",
                        "E200", ex.getMessage());
            }
        }

        res.sendRedirect("http://localhost:8081/w2018_apollo/question/"+question);

    }

}
