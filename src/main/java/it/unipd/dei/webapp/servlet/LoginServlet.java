package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.database.ReadUserDatabase;
import it.unipd.dei.webapp.resource.Message;
import it.unipd.dei.webapp.resource.User;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.crypto.dsig.DigestMethod;
import java.io.IOException;
import java.sql.SQLException;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * Servlet to execute the login procedure
 *
 * @author Apollo group
 * @version 1.00
 * @since 1.00
 */
public class LoginServlet extends AbstractDatabaseServlet{

    /**
     * Manages HTTP GET requests in the login procedure"
     *
     * @param req
     *            the request from the client.
     * @param res
     *            the response from the server.
     *
     * @throws ServletException
     *             if any problem occurs while executing the servlet.
     * @throws IOException
     *             if any problem occurs while communicating between the client
     *             and the server.
     */
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {
        req.getRequestDispatcher("/jsp/login-form.jsp").forward(req, res);
    }

    /**
     * Servlet to execute the login procedure
     *
     * @param req
     *            the HTTP request from the client.
     * @param res
     *            the HTTP response from the server.
     *
     * @throws ServletException
     *             if any error occurs while executing the servlet.
     * @throws IOException
     *             if any error occurs in the client/server communication.
     */
    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {
        String user_email = null;
        String password = null;

        User user  = null;
        Message m = null;

        user_email = req.getParameter("user_email");
        password = req.getParameter("password");


        boolean login_ok = false;

        try
        {
            ReadUserDatabase readUserDatabase = new ReadUserDatabase(getDataSource().getConnection(), user_email);
            user = readUserDatabase.readUser();
            //user is a resource fill with database user information
            if (user != null)
            {
                if (user.getPassword().compareTo(DigestUtils.md5Hex(password).toUpperCase()) == 0) {
                    req.getSession().setAttribute("user", user);
                    login_ok = true;
                } else {
                    //Wrong password
                }
            }
            else {
                //Username email doesn't exist!
                m = new Message("Login failed, wrong username or password. Please try again: ");
            }
        }
        catch (SQLException ex)
        {
            m = new Message("SQL ERROR!!");
        }

        if (login_ok){
            m = new Message("Login success!");
            req.setAttribute("message", m);

            Cookie nameCookie = new Cookie("username", user.getUsername());
            nameCookie.setPath("/");
            Cookie useremail = new Cookie("useremail", user_email);
            useremail.setPath("/");
            res.addCookie(nameCookie);
            res.addCookie(useremail);

            req.getRequestDispatcher("/jsp/index.jsp").forward(req, res);


        } else {
            m = new Message(String.format("Login wrong!", user_email),
                    "", "");
            req.setAttribute("message", m);
            req.getRequestDispatcher("/jsp/login-form.jsp").forward(req, res);
        }


    }
}