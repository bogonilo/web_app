<?xml version="1.0"?>
<!--
 
 Copyright 2018 University of Padua, Italy
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 
 Author: Apollo group
 Version: 1.0
 Since: 1.0
-->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>it.unipd.dei.webapp</groupId>

	<artifactId>w2018_apollo</artifactId>

	<version>1.00</version>

	<packaging>war</packaging>

	<!-- Project description elements -->
	<name>Apollo group homework project</name>

	<description>Application using REST and JSP to access a database.</description>

	<url>https://bitbucket.org/upd-dei-stud-prj/w2018_apollo/src/master/</url>

	<inceptionYear>2018</inceptionYear>

	<developers>
		<developer>
			<id>ab</id>
			<name>Adrian Benim</name>
		</developer>
		<developer>
			<id>lb</id>
			<name>Lorenzo Bogoni</name>
		</developer>
		<developer>
			<id>dd</id>
			<name>Daniele Del Degan</name>
		</developer>
		<developer>
			<id>mm</id>
			<name>Matteo Maso</name>
		</developer>
		<developer>
			<id>dp</id>
			<name>Davide Piona</name>
		</developer>
	</developers>

	<licenses>
		<license>
			<name>The Apache Software License, Version 2.0</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
			<distribution>repo</distribution>
		</license>
	</licenses>

	<organization>
		<name>Department of Information Engineering (DEI), University of Padua, Italy</name>
		<url>http://www.dei.unipd.it/en/</url>
	</organization>

	<!-- Build settings -->

	<!-- Specifies the encoding to be used for project source files -->
	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>
	
	<!-- Configuration of the default build lifecycle -->
	<build>
		<defaultGoal>compile</defaultGoal>
		
		<!-- source code folder -->
		<sourceDirectory>${basedir}/src/main/java</sourceDirectory>
		
		<!-- compiled code folder -->
		<directory>${basedir}/target</directory>
		
		<!-- name of the generated package -->
		<finalName>${project.artifactId}</finalName>

		<!-- configuration of the plugins for the different goals -->
		<plugins>
		
			<!-- compiler plugin: source and target code is for Java 1.8 -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.7.0</version>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
				</configuration>
			</plugin>
			
			
			<!-- javadoc plugin: output in the javadoc folder -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>3.0.0</version>
				<configuration>
					<reportOutputDirectory>${basedir}/javadoc</reportOutputDirectory>
					<author>true</author>
					<nosince>false</nosince>
					<show>protected</show>
				</configuration>
			</plugin>

			<!-- packager plugin: create a WAR file to be deployed -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-war-plugin</artifactId>
				<version>3.2.0</version>
				<configuration>
					<webXml>${basedir}/src/main/webapp/WEB-INF/web.xml</webXml>
				</configuration>
			</plugin>		
		</plugins>
		
		<!--  process resources before compilation and packaging -->
		<resources>
			
			<!--  copy CSS files to the target directory -->
			<resource>
				<targetPath>${basedir}/target/${project.artifactId}-${project.version}/css</targetPath>
				<directory>${basedir}/src/main/webapp/css</directory>
				<includes>
					<include>**/*.*</include>
				</includes>
			</resource>
		
			<!--  copy JS files to the target directory -->
			<resource>
				<targetPath>${basedir}/target/${project.artifactId}-${project.version}/js</targetPath>
				<directory>${basedir}/src/main/webapp/js</directory>
				<includes>
					<include>**/*.*</include>
				</includes>
			</resource>
		
			<!--  copy JSP files to the target directory -->
			<resource>
				<targetPath>${basedir}/target/${project.artifactId}-${project.version}/jsp</targetPath>
				<directory>${basedir}/src/main/webapp/jsp</directory>
				<includes>
					<include>**/*.*</include>
				</includes>
			</resource>
			
			<!--  copy img files to the target directory -->
			<resource>
				<targetPath>${basedir}/target/${project.artifactId}-${project.version}/img</targetPath>
				<directory>${basedir}/src/main/webapp/img</directory>
				<includes>
					<include>**/*.*</include>
				</includes>
			</resource>
			
			<!--  copy configuration files to the target directory -->
			<resource>
				<targetPath>${basedir}/target/${project.artifactId}-${project.version}/META-INF</targetPath>
				<directory>${basedir}/src/main/webapp/META-INF</directory>
				<includes>
					<include>**/*.*</include>
				</includes>
			</resource>
			
		</resources>

	</build>

	<!-- Dependencies -->
	<dependencies>
		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>javax.servlet-api</artifactId>
			<version>4.0.0</version>
			<scope>provided</scope>
		</dependency>
		
		<dependency>
			<groupId>org.postgresql</groupId>
			<artifactId>postgresql</artifactId>
			<version>42.2.2</version>
		</dependency>
		
		<dependency>
			<groupId>org.apache.tomcat</groupId>
			<artifactId>tomcat-jdbc</artifactId>
			<version>9.0.7</version>
			<scope>provided</scope>
		</dependency>
		
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-core</artifactId>
			<version>2.9.5</version>
		</dependency>
		
		<dependency>
			<groupId>jstl</groupId>
			<artifactId>jstl</artifactId>
			<version>1.2</version>
    	</dependency>

		<!-- https://mvnrepository.com/artifact/commons-codec/commons-codec -->
		<dependency>
			<groupId>commons-codec</groupId>
			<artifactId>commons-codec</artifactId>
			<version>1.9</version>
		</dependency>

	</dependencies>


</project>